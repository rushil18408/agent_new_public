package com.agent_new;

import android.app.ActivityManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.Adapter_Reminder;
import model.CandidateTask;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import services.TService;
import utils.AppPreference;
//made by rushil

public class RemindersActivity extends AppCompatActivity  implements RecruiterPositionInterface {
    public String jobid="nahiaayi";
    public Long reqruitmentid;
    public String url_api_deepti_2="https://hrbot.co/API/mtt/walkinapp/api/hiringRequirements/getAssignUsersList";
    public String LOG_TAG="RemindersActivity";
    public String res;
    public ArrayList<CandidateTask> recruiterList = new ArrayList<>();
    public Adapter_Reminder adapter_reminder;
    public String job_name;
    public String loc;
    public ArrayList<CandidateTask> filtered = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders);

        jobid = getIntent().getStringExtra("id");
        job_name = getIntent().getStringExtra("job");
        loc = getIntent().getStringExtra("loc");

        TextView select = findViewById(R.id.select);
        TextView locat = findViewById(R.id.location);
        select.setText(job_name);
        locat.setText(loc);

        reqruitmentid = AppPreference.getrecruitmentid(RemindersActivity.this);

        RecyclerView recyclerView = findViewById(R.id.rv_reminders);
//        CandidateTask candidateTask1 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask2 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask3 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask4 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask5 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask6 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask7 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask8 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask9 = new CandidateTask("Rushil Thareja", "TODAY");
//        CandidateTask candidateTask10 = new CandidateTask("Rushil Thareja", "TODAY");
//
//        recruiterList.add(candidateTask1);
//        recruiterList.add(candidateTask2);
//        recruiterList.add(candidateTask3);
//        recruiterList.add(candidateTask4);
//        recruiterList.add(candidateTask5);
//        recruiterList.add(candidateTask6);
//        recruiterList.add(candidateTask7);
//        recruiterList.add(candidateTask8);
//        recruiterList.add(candidateTask9);
//        recruiterList.add(candidateTask10);
        final ImageButton home = (ImageButton) findViewById(R.id.piche);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        try {
            post(url_api_deepti_2,jobid,reqruitmentid);
        } catch (IOException e) {
            e.printStackTrace();
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        adapter_reminder = new Adapter_Reminder(this,filtered,this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter_reminder);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) findViewById(R.id.dhoond2);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    filtered.clear();
                    for (CandidateTask job : recruiterList){
                        filtered.add(job);
                    }
//                    filteredRecruiterList = recruiterList;
                } else {
                    filtered.clear();
                    for (CandidateTask job : recruiterList) {

                        if(job.getName().toLowerCase().startsWith(query.toLowerCase())){
                            filtered.add(job);
//                            ids.add()

                        }
                    }
                }
                adapter_reminder.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.isEmpty()) {
                    filtered.clear();
                    for (CandidateTask job : recruiterList){
                        filtered.add(job);
                    }
//                    filteredRecruiterList = recruiterList;
                } else {
                    filtered.clear();
                    for (CandidateTask job : recruiterList) {

                        if(job.getName().toLowerCase().startsWith(query.toLowerCase())){
                            filtered.add(job);
//                            ids.add()

                        }
                    }
                }
                adapter_reminder.notifyDataSetChanged();
                return false;
            }
        });

    }

    @Override
    public void getRecruiterItemPosition(int position, String bleh) {
//        Toast.makeText(this, "Position from adapter is " + position + bleh, Toast.LENGTH_SHORT).show();
        long call = filtered.get(position).getNumber();
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + call));
        startActivity(intent);
        if(!isServiceRunning(TService.class)) {
            final Intent in = new Intent(this, TService.class);
            in.putExtra("stop", "true");
            in.putExtra("id",AppPreference.getrecruitmentid(this));
            startService(in);
        }
        Intent inte = new Intent(RemindersActivity.this,Smssender.class);

        inte.putExtra("phone",call);
        inte.putExtra("name",filtered.get(position).getName());
    }

    private void post(String url, String jobid,Long reqruitmentid) throws IOException {
        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        Log.d("yuyu",jobid);
        RequestBody formBody = new FormBody.Builder()
                .add("rid",jobid)
                .add("recruitmentId",reqruitmentid.toString())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                datasetterfromjson(res);
                Log.d(LOG_TAG, res);

            }
        });

    }



    public void datasetterfromjson(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray jobs = jsonObj.optJSONArray("response");
            String name;
            String current_job;
            String current_city;
            String home_city;
            String salary;
            String qualification;
            Long phone;
            for (int i = 0; i < jobs.length()-1; i++){
                JSONObject employee_data = jobs.optJSONObject(i);
                name = employee_data.optString("userName");
                current_job = employee_data.optString("currentEmployer");
                phone = employee_data.optLong("phone");
                current_city = employee_data.optString("currentLoc");
                home_city = employee_data.optString("homecity");
                salary = employee_data.optString("currentCTC");
                qualification = employee_data.optString("qualification");

                Log.d("brittoo",phone+"");

                if(name=="null" || name== ""){
                    name="-";
                }
                if(current_city=="null" || current_city==""){
                    current_city="-";
                }
                if(current_job=="null" || current_job==""){
                    current_job="-";
                }
                if(home_city=="null" || home_city==""){
                    home_city="-";
                }
                if(salary=="null" || salary ==""){
                    salary="-";
                }
                if(qualification=="null" || qualification==""){
                    qualification="-";
                }
                CandidateTask candidateTask = new CandidateTask(name, current_job,current_city,home_city,salary,qualification,phone);
                recruiterList.add(candidateTask);
                filtered.add(candidateTask);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter_reminder.notifyDataSetChanged();
                            }
                        });
                    }
                });
            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
