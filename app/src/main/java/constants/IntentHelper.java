
    package constants;


    public class IntentHelper {
        public static final String PROFILE_COMMENT = "profile";
        public static final String JOB_COMMENT = "job";
        public static final String INTENT_JOBS_IDS = "intent_jobs_ids";
        public static final String INTENT_JOB_ID = "Id";
        public static final String INTENT_NOTIFICATION_ID = "notificationId";
        public static final String INTENT_FIRST_LOGIN = "firstLogin";
        public static final String INTENT_QUERY = "query";
        public static final String INTENT_SEARCH_SCREEN_OPEN = "search_screen_open";
        public static final String INTENT_CITY_ID = "intent_city_id";
        public static final String INTENT_ATTACHMENT_ID = "attachmentId";
        public static final String OPEN_SUGGESTED_JOBS = "open_suggested_jobs";
        public static final String OPEN_MY_SKILL = "my_skill";
        public static final String OPEN_NOTIFICATION_SCREEN = "notification_screen";
        public static final String SEARCH_SCREEN = "search_screen";
        public static final String INTENT_OPEN_TEST = "intent_open_test";
        public static final String INTENT_COUPONS_MESSAGE = "intent_coupons_message";
        public static final String INTENT_SCREEN_NAME_TO_OPEN_FROM_MAIN = "intent_screen_name_to_open_from_main";
        public static final String INTENT_COUPONS_TITLE = "intent_coupons_title";
        public static final String INTENT_COUPONS_LOGO = "intent_coupons_logo";
        public static final String INTENT_COUPONS_SKILL_ID = "intent_coupons_skill_id";
        public static final String INTENT_RECOMMENDATION = "intent_recommended";
        public static final String INTENT_RECOMMENDATION_DATE = "intent_recommendation_date";
        public static final String INTENT_TAG_REMOTE_CONFIG_PARAM = "intent_tag_remote_config_param";
        public static final String INTENT_ALREADY_SELECTED_TAGS = "intent_already_selected_tags";
        public static final String INTENT_AFTER_SELECTED_TAGS = "intent_after_selected_tags";
        public static final String INTENT_TAGS_SELECTION_TOP_HEADER = "intent_tags_selection_top_header";
        public static final String INTENT_JOB_ID_FOR_CONTEXT_ID = "intent_job_id_for_context_id";
        public static final String INTENT_COMING_FROM_SCREEN_CONTEXT = "intent_coming_from_screen_context";
        public static final String INTENT_NAME_SCREEN_COMPLETED = "intent_name_screen_completed";
        public static final String INTENT_VIDEO_ID = "intent_video_id";
        public static final String INTENT_FOR_DEEP = "intent_for_deep";
        public static final String INTENT_CITY_NEED_TO_ASK = "intent_city_need_to_ask";
        public static final String INTENT_GIF_URL = "intent_gif_url";
        public static final String INTENT_RESULT_MESSAGE = "intent_result_message";
        public static final String INTENT_RESULT_IMAGE_URL = "intent_result_image_url";
        public static final String INTENT_FEED_BACK_DATA = "intent_feed_back_data";
        public static final String INTENT_FEED_BACK_TYPE = "intent_feed_back_type";
        public static final String INTENT_CALL_TRACK_ID = "intent_call_track_id";
        public static final String INTENT_CALL_PHONE_NUMBER = "intent_call_phone_number";
        public static final String INTENT_ACTION_TO_OPEN_SCREEN = "intent_action_to_open_screen";
        public static final String INTENT_SHAREABLE_LINK = "intent_shareable_link";
        public static final String INTENT_ACTION_FROM_NOTIFICATION_OR_DEEP_LINKING = "intent_action_from_notification_or_deep_linking";
        public static final String INTENT_QUIZ_ID = "intent_quiz_id";
        public static final String INTENT_QUIZ_BUNDLE_ID = "intent_quiz_bundle_id";
        public static final String INTENT_OPEN_FEEDBACK_DIALOG = "intent_open_feedback_dialog";
        public static final String INTENT_OPEN_INTERVIEW_FEEDBACK = "intent_open_interview_feedback";
        public static final String INTENT_RUN_TIME_PERMISSION_ID = "intent_run_time_permission_id";
        public static final String INTENT_SELECTED_SKILLS = "intent_selected_skills";
        public static final String INTENT_IS_NEW_SKILLS_ADDED = "intent_is_new_skills_added";
        public static final String INTENT_SKILL_CATEGORY_ID = "intent_skill_category_id";
        public static final String INTENT_SKILL_CATEGORY_NAME = "intent_skill_category_name";

        public static final String INTENT_TEST_ID = "intent_test_id";

        public static final String INTENT_IS_QUESTION_FIXED = "intent_is_question_fixed";
        public static final String INTENT_TOTAL_QUESTIONS = "intent_total_questions";
        public static final String INTENT_TOTAL_SOLVED_QUESTIONS = "intent_total_solved_questions";
        public static final String INTENT_PREVIOUS_SCREEN_GAVE_DATA_FOR_MESSAGES = "intent_previous_screen_gave_data_for_messages";
        public static final String INTENT_IS_START_SCREEN = "intent_is_start_screen";
        public static final String INTENT_IS_FROM_DEFERRED = "intent_is_from_deferred";
        public static final String INTENT_TO_GET_USER_DATA_CONFIG = "intent_to_get_user_data_config";
        public static final String INTENT_TO_MUST_GET_USER_DATA_CONFIG = "intent_to_must_get_user_data_config";
        public static final String INTENT_START_SERVICE_FOR_DETAILS = "intent_start_service_for_details";
        public static final String INTENT_ASSESSMENT_DATA = "intent_assessment_data";
        public static final String INTENT_STATE_CITY_DATA = "intent_state_city_data";
        public static final String INTENT_STATE_DATA = "intent_state_data";
        public static final String INTENT_COUNTRY_DATA = "intent_country_data";
        public static final String INTENT_CATEGORY_IDS = "intent_category_ids";
        public static final String INTENT_JOB_DATA_FROM_GCM = "intent_job_data_from_gcm";
        public static final String INTENT_APP_INDEXING = "intent_app_indexing";
        public static final String INTENT_APP_INDEXING_TITLE = "intent_app_indexing_title";
        public static final String INTENT_APP_INDEXING_MESSAGE = "intent_app_indexing_message";
        public static final String INTENT_APP_INDEXING_DEEP_URL = "intent_app_indexing_deep_url";
        public static final String INTENT_APP_INDEXING_LEADERBOARD_REPORT_CARD = "intent_app_indexing_leaderboard_report_card";
        public static final String INTENT_APP_INDEXING_FOR_MULTIPLE_JOBS = "intent_app_indexing_for_multiple_jobs";
        public static final String INTENT_SEND_CALL_INFORMATION = "intent_send_call_information";
        public static final String INTENT_EMP_NUMBER = "intent_emp_number";


        public static final String INTENT_SHARE_FB = "intent_share_fb";
        public static final String INTENT_SHARE_WA = "intent_share_wa";
        public static final String INTENT_SHARE_CTCB = "intent_share_ctcb";
        public static final String INTENT_SHARE_AS = "intent_share_as";


        public static final String INTENT_VIEW_PAGER_PAGE_NO = "intent_view_pager_page_no";
        public static final String INTENT_SHOW_VIEW_PAGER = "intent_show_view_pager";
        public static final String INTENT_JOB_DETAIL_OPEN = "intent_job_detail_open";


        public static final String INTENT_DOWNLOAD_BROADCAST = "intent_download_broadcast";
        public static final String INTENT_SUCCESSFULLY_DOWNLOAD_BROADCAST = "intent_successfully_download_broadcast";


        public static final String VIDEO_APP = "video_app";
        public static final String CALL_FEED_BACK = "call_feed_back";
        public static final String MY_REPORT_CARD = "my_report_card";


        public static final String INTENT_START_BUTTON = "intent_start_button";
        public static final String INTENT_START_MESSAGE = "intent_start_message";
        public static final String INTENT_START_ICON_FONT = "intent_start_icon_font";
        public static final String INTENT_END_BUTTON = "intent_end_button";
        public static final String INTENT_END_MESSAGE = "intent_end_message";
        public static final String INTENT_END_ICON_FONT = "intent_end_icon_font";
        public static final String INTENT_VIEW_PAGER_PAGE_CHANGE = "intent_view_pager_page_change";


        public static final String INTENT_SUB_SKILL_CATEGORY_ID = "intent_sub_skill_category_id";


        public static final String INTENT_SUB_SKILL_CATEGORY_NAME = "intent_sub_skill_category_name";


        public static final String INTENT_REQUESTED_API = "intent_requested_api";
        public static final String INTENT_REQUESTED_API_SEND_DATA = "intent_requested_api_send_data";
        public static final String INTENT_REQUESTED_API_SEND_HEADER = "intent_requested_api_send_header";
        public static final String INTENT_REQUESTED_API_TYPE = "intent_requested_api_type";
        public static final String INTENT_REQUESTED_API_DATA_TYPE = "intent_requested_api_data_type";

        public static final String INTENT_SAVE_AND_SEND_DATA = "intent_save_and_send_data";
        public static final String INTENT_SEND_DATA = "intent_send_data";

        public static final String INTENT_CAMPAIGN_ID = "intent_campaign_id";

        public static final String INTENT_TEST_CATEGORY = "intent_test_category";
        public static final String INTENT_SKILL_ID = "intent_skill_id";
        public static final String INTENT_RESULT_RESULT_URL = "intent_result_result_url";
        public static final String INTENT_PDF_SUCCESSFULLY_DOWNLOAD_BROADCAST = "intent_pdf_successfully_download_broadcast";
        public static final String INTENT_DOCX_SUCCESSFULLY_DOWNLOAD_BROADCAST = "intent_docx_successfully_download_broadcast";
        public static final String INTENT_PNG_SUCCESSFULLY_DOWNLOAD_BROADCAST = "intent_png_successfully_download_broadcast";
        public static final String INTENT_PARAGRAPH_ID = "intent_paragraph_id";
        public static final String INTENT_INSTRUCTION_DATA = "intent_instruction_data";
        public static final String INTENT_OTP_MOBILE = "user_mobile";
    }

