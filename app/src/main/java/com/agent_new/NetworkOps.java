package com.agent_new;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import model.RecruiterTask;

public class NetworkOps {

    Context mContext;
    ApiRequestResult mResultInterface;
    ArrayList<RecruiterTask> recruiterTaskArrayList = new ArrayList<>();

    public NetworkOps(Context context, ApiRequestResult requestResult) {
        mContext = context;
        mResultInterface = requestResult;
    }



    public void datasetterfromjson1(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray jobs = jsonObj.optJSONArray("response");
            String head;
            String body;
            for (int i = 0; i < jobs.length(); i++) {
                JSONObject jobdata = jobs.optJSONObject(i);
                String requirements = jobdata.optString("requirements");
                String[] parts = requirements.split("\n");
                head = parts[0];
                body = parts[1];
                Log.d("crm", head + " " + body);
                RecruiterTask recruiterTask = new RecruiterTask(head, body, "3","123");
                recruiterTaskArrayList.add(recruiterTask);
                mResultInterface.getApiRequestResult(recruiterTaskArrayList);
//                recruiterAdapter.notifyItemInserted(i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
