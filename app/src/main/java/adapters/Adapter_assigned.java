package adapters;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.agent_new.R;
import com.agent_new.Readmore;
import com.agent_new.RecruiterPositionInterface;
import com.agent_new.RemindersActivity;

import java.util.ArrayList;

import model.AssignedTask;
import services.TService;

public class Adapter_assigned extends RecyclerView.Adapter<Adapter_assigned.ReminderHolder>{

    private Context mContext;
    private ArrayList<AssignedTask> mRecruiterList;
    private RecruiterPositionInterface mPositionInterface;

    public Adapter_assigned(Context context, ArrayList<AssignedTask> recruiterList,RecruiterPositionInterface positionInterface) {
        mContext = context;
        mRecruiterList = recruiterList;
        mPositionInterface = positionInterface;

    }

    @NonNull
    @Override
    public ReminderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.assigned_row,parent,false);
        return new ReminderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReminderHolder holder, int position) {
        final AssignedTask candidateTask = mRecruiterList.get(position);
        holder.name.setText(candidateTask.name);
        holder.status.setText(candidateTask.status);
        holder.post.setText(candidateTask.post);
        holder.loc.setText(candidateTask.loc);
        holder.type.setText(candidateTask.type);

        String from = candidateTask.from_where;
        if(from.equals("plugin")){
            holder.bot.setVisibility(View.VISIBLE);
            holder.facebook.setVisibility(View.INVISIBLE);
            holder.whatsapp.setVisibility(View.INVISIBLE);
        }else if(from.equals("whatsappbot")){
            holder.bot.setVisibility(View.INVISIBLE);
            holder.facebook.setVisibility(View.INVISIBLE);
            holder.whatsapp.setVisibility(View.VISIBLE);
        }else if(from.equals("facebook")){
            holder.bot.setVisibility(View.INVISIBLE);
            holder.facebook.setVisibility(View.VISIBLE);
            holder.whatsapp.setVisibility(View.INVISIBLE);
        }else{
            holder.bot.setVisibility(View.INVISIBLE);
            holder.facebook.setVisibility(View.INVISIBLE);
            holder.whatsapp.setVisibility(View.INVISIBLE);
        }
//
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + candidateTask.getPhone()));
                mContext.startActivity(intent);
                if(!isServiceRunning(TService.class)) {
                    final Intent in = new Intent(mContext, TService.class);
                    in.putExtra("stop", "true");
                    mContext.startService(in);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(mContext, Readmore.class);
                in.putExtra("name",candidateTask.getName());
                in.putExtra("phone",candidateTask.getPhone());
                in.putExtra("uid",candidateTask.getUid());
//                in.putExtra("job",filteredRecruiterList.get(position).getHead() + " " + filteredRecruiterList.get(position).getBody());
//                in.putExtra("loc",filteredRecruiterList.get(position).getPending());
                mContext.startActivity(in);
                mPositionInterface.getRecruiterItemPosition(holder.getAdapterPosition(),"LOL");
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mRecruiterList != null) {
            return mRecruiterList.size();
        } else {
            return 0;
        }
    }

    class ReminderHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView status;
        final TextView loc;
        final TextView post;
        final ImageView call;
        final ImageView whatsapp;
        final ImageView facebook;
        final ImageView bot;
        final TextView type;

        ReminderHolder(View itemView) {
            super(itemView);
            type =  itemView.findViewById(R.id.type_assigned);
            whatsapp = itemView.findViewById(R.id.reached_whatsapp);
            facebook = itemView.findViewById(R.id.reached_facebook);
            bot = itemView.findViewById(R.id.reached_bot);
            call = itemView.findViewById(R.id.call_button_assg);
            name = itemView.findViewById(R.id.assg_name);
            status = itemView.findViewById(R.id.interview_date);
            loc = itemView.findViewById(R.id.interview_loc);
            post = itemView.findViewById(R.id.interview_post);
//                Typeface typeface = Typeface.createFromFile();
//                pending.setTypeface(typeface);
        }
    }
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



}