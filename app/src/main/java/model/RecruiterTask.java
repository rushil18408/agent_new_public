package model;

public class RecruiterTask {
    public String head ;
    public String body ;
    public String pending ;
    public String jobid;

    public RecruiterTask(String head , String body , String pending,String jobid){
        this.head = head;
        this.body = body;
        this.pending = pending;
        this.jobid = jobid;

    }
    public String getHead(){
        return(this.head);
    }
    public String getBody(){
        return(this.body);
    }

    public String getJobid() {
        return this.jobid;
    }

    public String getPending(){
        return(this.pending);
    }
    public void setHead(String h){
        this.head = h ;
    }
    public void setBody(String h){
        this.body = h ;
    }
    public void setPending(String h){
        this.pending = h ;
    }
}

