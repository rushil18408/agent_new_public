package com.agent_new;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.agent_new.ProfileActivity;
import com.agent_new.R;

import constants.IntentHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import services.TService;
import utils.AppPreference;


public class ActivityFollow_up extends AppCompatActivity {
    EditText job_comments;
    String candidate_uid;
    String head,question,url,hint,next,key;
    String old = " ";
    String phone;
    TextView header,boder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followup_next);

        head = getIntent().getStringExtra("name");
        question = getIntent().getStringExtra("question");
        url = getIntent().getStringExtra("update_url");
        hint = getIntent().getStringExtra("hint");
        next = getIntent().getStringExtra("next");
        key = getIntent().getStringExtra("key");


        phone = AppPreference.gettempcandphone(this);

        ImageButton call = findViewById(R.id.call_button_followup);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentu = new Intent(Intent.ACTION_DIAL);
                intentu.setData(Uri.parse("tel:" + phone));
                startActivity(intentu);
                if(!isServiceRunning(TService.class)) {
                    final Intent in = new Intent(ActivityFollow_up.this, TService.class);
                    in.putExtra("stop", "true");
                    startService(in);
                }
            }
        });

        TextView name_feed = findViewById(R.id.name_read_followup_next);
        name_feed.setText(AppPreference.gettempname(this));

        candidate_uid = AppPreference.gettempuid(ActivityFollow_up.this);

        job_comments = findViewById(R.id.comments_followup_next);
        job_comments.setHint(AppPreference.getcomment(this, candidate_uid, IntentHelper.JOB_COMMENT));

        header = findViewById(R.id.header_followup_next);
        boder = findViewById(R.id.folloup_header);

        header.setText(question);
        boder.setText(hint);

        old = AppPreference.getcomment(this, candidate_uid, IntentHelper.JOB_COMMENT);
        old=old+" ";
        job_comments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String newer = s.toString();
                String finals;
                finals = old + newer;
                AppPreference.setcomment(ActivityFollow_up.this, candidate_uid, IntentHelper.JOB_COMMENT, finals);
                job_comments.setHint(finals);
                job_comments.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageButton back = findViewById(R.id.back_read_followup_next);
        CircleImageView profile = findViewById(R.id.readmore_profile_followup_next);
        final Button nexti = findViewById(R.id.save_next_followup_next);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chahat = new Intent(ActivityFollow_up.this, ProfileActivity.class);
                startActivity(chahat);
            }
        });
        nexti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String response;
                response = job_comments.getText().toString();
                Intent inl = new Intent(ActivityFollow_up.this,ActivityFeedback.class);
                inl.putExtra("response", response);
                inl.putExtra("update_url", url);
                inl.putExtra("key", key);
                inl.putExtra("next",next);
                startActivity(inl);
            }
        });



    }
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public void send_whatsapp(String number, String text) {
        String formattedNumber;
        if (number.length() == 10) {
            formattedNumber = "91" + number;
        } else {
            formattedNumber = number;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + formattedNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send_custom_sms(String phoneNo) {
        Intent sInt = new Intent(Intent.ACTION_VIEW);
        sInt.putExtra("sms_body", " ");
        sInt.putExtra("address", phoneNo);
        sInt.setType("vnd.android-dir/mms-sms");
        startActivity(sInt);

    }

    public void send_SMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
    public void composeEmail(String[] addresses, String subject, String saman) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, saman);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
