package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agent_new.R;
import com.agent_new.RecruiterPositionInterface;

import java.util.ArrayList;

import model.CandidateTask;

public class Adapter_Reminder extends RecyclerView.Adapter<Adapter_Reminder.ReminderHolder>{

    private Context mContext;
    private ArrayList<CandidateTask> mRecruiterList;
    private RecruiterPositionInterface mPositionInterface;

    public Adapter_Reminder(Context context, ArrayList<CandidateTask> recruiterList,RecruiterPositionInterface positionInterface) {
        mContext = context;
        mRecruiterList = recruiterList;
        mPositionInterface = positionInterface;

    }

    @NonNull
    @Override
    public ReminderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.candidate_row,parent,false);
        return new ReminderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReminderHolder holder, int position) {
        CandidateTask candidateTask = mRecruiterList.get(position);
        holder.name.setText(candidateTask.name);
        holder.day.setText(candidateTask.day);
        holder.current_city.setText(candidateTask.current_city);
        holder.home_city.setText(candidateTask.home_city);
        holder.ctc.setText(candidateTask.ctc);
        holder.qualification.setText(candidateTask.qualification);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPositionInterface.getRecruiterItemPosition(holder.getAdapterPosition(),"LOL");
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mRecruiterList != null) {
            return mRecruiterList.size();
        } else {
            return 0;
        }
    }

    class ReminderHolder extends RecyclerView.ViewHolder {

        final TextView name;
        final TextView day;
        final TextView current_city;
        final TextView home_city;
        final TextView ctc;
        final TextView qualification;

        ReminderHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.msg);
            day = itemView.findViewById(R.id.day);
            current_city = itemView.findViewById(R.id.current_city);
            home_city = itemView.findViewById(R.id.home_city);
            ctc = itemView.findViewById(R.id.ctc);
            qualification = itemView.findViewById(R.id.qualification);
//                Typeface typeface = Typeface.createFromFile();
//                pending.setTypeface(typeface);
        }
    }

}