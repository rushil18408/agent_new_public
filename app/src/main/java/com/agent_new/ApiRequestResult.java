package com.agent_new;

import java.util.ArrayList;

import model.RecruiterTask;

public interface ApiRequestResult {
    void getApiRequestResult(ArrayList<RecruiterTask> recruiterTasks);
}
