package utils;

import android.app.Application;

public class Globals extends Application {
    private int iteration_opened = 0;

    public int getIteration_opened(){
        return this.iteration_opened;
    }

    public void setIteration_opened(int d){
        this.iteration_opened=d;
    }
}
