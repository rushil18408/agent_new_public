package com.agent_new;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import de.hdodenhof.circleimageview.CircleImageView;
import services.TService;
import utils.AppPreference;

public class ActivityDate extends AppCompatActivity {
    String head,question,url,hint,next,key,phone;
    private View popupContentView, popupContentView2;
    private PopupWindow popupWindow, popupwindowmail;
    String email_receiver = "rushilthareja@gmail.com";
    String email_subject = "DONT WORRY IT ONLY FOR TESTING";
    String body = "aur batao kya hal chal hain ? sab badiya ?";
    String resp[] = new String[1];
    String whatsapp_number = "9354464293";
    String LOG_TAG = "activity_readmore";
    String whatsapp_content;
    String body_share = "head";
    String body_subject = "body";
    String botlink = "botlink";
    String company_and_job = "company and job";
    String interview_details = "interview details";
    private View main;
    private ImageButton whatsapp_button, sms_button,goback;
    private Button save_next;
    private View bottombar, bottomconstraint;
    private String botlink_subject_mail = "subject for botlink";
    private String company_job_subject_mail = "subject for company job details";
    private String interview_confirmation_subject_mail = "subject for confirmation";
    private String customise_subject_mail = " ";
    private DatePicker datePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_picker);





        phone = AppPreference.gettempcandphone(this);
        ImageButton call = findViewById(R.id.call_button_date_picker);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentu = new Intent(Intent.ACTION_DIAL);
                intentu.setData(Uri.parse("tel:" + phone));
                startActivity(intentu);
                if(!isServiceRunning(TService.class)) {
                    final Intent in = new Intent(ActivityDate.this, TService.class);
                    in.putExtra("stop", "true");
                    startService(in);
                }
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        head = getIntent().getStringExtra("name");
        question = getIntent().getStringExtra("question");
        url = getIntent().getStringExtra("update_url");
        hint = getIntent().getStringExtra("hint");
        next = getIntent().getStringExtra("next");
        key = getIntent().getStringExtra("key");

        datePicker = findViewById(R.id.datePicker1);
        save_next = findViewById(R.id.button_save_next);

        save_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String response;
                response = datePicker.getDayOfMonth()+"/"+ (datePicker.getMonth() + 1)+"/"+datePicker.getYear();
                Intent inl = new Intent(ActivityDate.this,ActivityFeedback.class);
                inl.putExtra("response", response);
                inl.putExtra("update_url", url);
                inl.putExtra("key", key);
                inl.putExtra("next", next);
                startActivity(inl);
            }
        });

        TextView name_feed = findViewById(R.id.name_read_date_picker);
        name_feed.setText(AppPreference.gettempname(this));

        ImageButton back = findViewById(R.id.back_read_date_picker);
        CircleImageView profile = findViewById(R.id.date_picker_profile);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chahat = new Intent(ActivityDate.this, ProfileActivity.class);
                startActivity(chahat);
            }
        });
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupContentView = layoutInflater.inflate(R.layout.message_popup, null);
        popupWindow = new PopupWindow(this);
        popupWindow.setContentView(popupContentView);
        popupWindow.setWidth((int) (width * 0.7));
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(null);

        LayoutInflater layouter = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupContentView2 = layouter.inflate(R.layout.message_pop_readmore, null);
        popupwindowmail = new PopupWindow(this);
        popupwindowmail.setContentView(popupContentView2);
        popupwindowmail.setWidth((int) (width * 0.7));
        popupwindowmail.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupwindowmail.setFocusable(true);
        popupwindowmail.setBackgroundDrawable(null);

        main = findViewById(R.id.main_layout_readmore);
        ImageButton img_button = findViewById(R.id.mail_date_picker);
        img_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupwindowmail.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                resp[0] = email_receiver;
                Button option_botlink = popupContentView2.findViewById(R.id.Share_Bot_link_with_Candidate_mail);
                Button option_details = popupContentView2.findViewById(R.id.Share_Company_and_Job_Details_mail);
                Button option_interview = popupContentView2.findViewById(R.id.Share_Interview_Details_mail);
                Button option_Customise = popupContentView2.findViewById(R.id.Customise_Message_mail);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = company_and_job;
                        popupWindow.dismiss();
                        composeEmail(resp, company_job_subject_mail, whatsapp_content);

                    }

                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        composeEmail(resp, botlink_subject_mail, whatsapp_content);

                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = interview_details;
                        popupWindow.dismiss();
                        composeEmail(resp, interview_confirmation_subject_mail, whatsapp_content);

                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        composeEmail(resp, customise_subject_mail, whatsapp_content);


                    }
                });


            }
        });
        whatsapp_button = findViewById(R.id.whatsapp_date_picker);
        whatsapp_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                Button option_botlink = popupContentView.findViewById(R.id.Share_Bot_link_with_Candidate);
                Button option_details = popupContentView.findViewById(R.id.Share_Company_and_Job_Details);
                Button option_interview = popupContentView.findViewById(R.id.Share_Interview_Details);
                Button option_Customise = popupContentView.findViewById(R.id.Customise_Message);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = company_and_job;
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = interview_details;
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);

                    }
                });

            }
        });
        sms_button = findViewById(R.id.message_date_picker);
        sms_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                Button option_botlink = popupContentView.findViewById(R.id.Share_Bot_link_with_Candidate);
                Button option_details = popupContentView.findViewById(R.id.Share_Company_and_Job_Details);
                Button option_interview = popupContentView.findViewById(R.id.Share_Interview_Details);
                Button option_Customise = popupContentView.findViewById(R.id.Customise_Message);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = company_and_job;
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = interview_details;
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        send_custom_sms(whatsapp_number);

                    }
                });


//                send_SMS(whatsapp_number,whatsapp_content);
            }
        });






    }
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public void send_whatsapp(String number, String text) {
        String formattedNumber;
        if (number.length() == 10) {
            formattedNumber = "91" + number;
        } else {
            formattedNumber = number;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + formattedNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send_custom_sms(String phoneNo) {
        Intent sInt = new Intent(Intent.ACTION_VIEW);
        sInt.putExtra("sms_body", " ");
        sInt.putExtra("address", phoneNo);
        sInt.setType("vnd.android-dir/mms-sms");
        startActivity(sInt);

    }

    public void send_SMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
    public void composeEmail(String[] addresses, String subject, String saman) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, saman);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
