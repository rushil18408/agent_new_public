package com.agent_new;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.RecruiterAdapter;
import model.RecruiterInfo;
import model.RecruiterTask;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import services.CheckAndSend;
import services.FileDeleter;
import services.TService;
import utils.AppPreference;

public class MainActivity extends AppCompatActivity implements RecruiterPositionInterface{
    public static final String TASK_TAG_PERIODIC = "periodic";
    private static final int LOADER_ID = 911;
    public String recruiter_info_url_api = "http://159.89.162.247:8080/walkinapp/api/usersvc/getUserDetailByContactNo";
    public String api1_url = "https://hrbot.co/hiringapp/listofRequirementsRoleBased.php";
    public int iter;
    public String picurl;
    public ImageView profile_pic;
    public RecruiterInfo info;
    public String res;
    //    public long recruiternumber = Long.parseLong("9625322065");
    public long recruiternumber;
    public String LOG_TAG = MainActivity.class.getSimpleName();
    public Long userid;
    public ArrayList<RecruiterTask> recruiterList = new ArrayList<>();
    public RecruiterAdapter recruiterAdapter;
    LottieAnimationView animationView;
    NavigationView mNavigationView;
    TextView sal;
    private int REQUEST_ALL = 45;
    private GcmNetworkManager mGcmNetworkManager;
    public ArrayList<String> idList = new ArrayList<>();
    public Intent intent;
    public SearchView searchView;
    public ArrayList<RecruiterTask> filteredRecruiterList = new ArrayList<>();
    public String class_name="main";


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
//        if (iter != 0)
//            Picasso.with(MainActivity.this).load(picurl).into(profile_pic);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        iter = AppPreference.getiter(MainActivity.this);
        setContentView(R.layout.activity_main_drawer);
        recruiternumber = Long.parseLong(AppPreference.getPhone(this));

//        recruiternumber = Long.parseLong("9205126081");



        animationView = findViewById(R.id.loading);
        mGcmNetworkManager = GcmNetworkManager.getInstance(this);
        RecyclerView recyclerView = findViewById(R.id.rv_main);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        recruiterAdapter = new RecruiterAdapter(this, filteredRecruiterList, this);
        recyclerView.setAdapter(recruiterAdapter);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.dhoond);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);



        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    filteredRecruiterList.clear();
                    for (RecruiterTask job : recruiterList){
                        filteredRecruiterList.add(job);
                    }
//                    filteredRecruiterList = recruiterList;
                } else {
                    filteredRecruiterList.clear();
                    for (RecruiterTask job : recruiterList) {

                        if(job.getHead().toLowerCase().startsWith(query.toLowerCase())){
                            filteredRecruiterList.add(job);
//                            ids.add()

                        }
                    }
                }
                recruiterAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.isEmpty()) {
                    filteredRecruiterList.clear();
                    for (RecruiterTask job : recruiterList){
                        filteredRecruiterList.add(job);
                    }
//                    filteredRecruiterList = recruiterList;
                } else {
                    filteredRecruiterList.clear();
                    for (RecruiterTask job : recruiterList) {

                        if(job.getHead().toLowerCase().startsWith(query.toLowerCase())){
                            filteredRecruiterList.add(job);
//                            ids.add()

                        }
                    }
                }
                recruiterAdapter.notifyDataSetChanged();
                return false;
            }
        });
//        NetworkOps networkOps = new NetworkOps(this,this);
//        networkOps.datasetterfromjson1();
        if (iter == 0) {
            try {
                Log.d("iter", "is" + iter);
                post(recruiter_info_url_api, recruiternumber);

                AppPreference.setIter(MainActivity.this, iter);
                Intent in = new Intent(this, SplashScreen.class);
                startActivityForResult(in, REQUEST_ALL);


            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "COULD NOT LOAD DATA HIT BUTTON", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }

//        RecruiterTask recruiterTask = new RecruiterTask("Reminders", "Remind about Interviews Today", "3");
//        RecruiterTask recruiterTask = new RecruiterTask("Post Interviews", "Take follow ups after Interview", "3");
//        RecruiterTask recruiterTask3 = new RecruiterTask("Assigned Candidates", "New Candidates to be Called", "3");
//        RecruiterTask recruiterTask4 = new RecruiterTask("Pending Candidates", "Pending Candidates", "3");
//        RecruiterTask recruiterTask5 = new RecruiterTask("Rejected Candidates", "candidates to be informed", "0");
//        RecruiterTask recruiterTask6 = new RecruiterTask("Special Cases", "Read Comments", "0");
//        recruiterList = new ArrayList<>();
//        recruiterList.add(recruiterTask1);
//        recruiterList.add(recruiterTask2);
//        recruiterList.add(recruiterTask3);
//        recruiterList.add(recruiterTask4);
//        recruiterList.add(recruiterTask5);
//        recruiterList.add(recruiterTask6);

        startPeriodicTaskFileUpload();
        final NavigationView navView = findViewById(R.id.nav_view);
        View headerView = navView.getHeaderView(0);
        final Switch scroll = headerView.findViewById(R.id.callrec);
        final FloatingActionButton stop = findViewById(R.id.stop);
        scroll.setChecked(false);

        Button goto_next1 = headerView.findViewById(R.id.button_todo_main);
        Button goto_next2 = headerView.findViewById(R.id.button_crm_main);

        goto_next1.setVisibility(View.VISIBLE);
        goto_next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenti = new Intent(MainActivity.this,AssignedcandidatesActivity.class);
                startActivity(intenti);
                finish();
            }
        });

//        if(scroll.isChecked()){
//            if(iter==0){
//                intent = new Intent(this, TService.class);
//                intent.putExtra("stop", "false");
//                startService(intent);
//                stop.show();
//
//            }
//        }

        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!scroll.isChecked()){
                    stopService(intent);
                    stop.hide();

                }else if(scroll.isChecked()){
                    intent = new Intent(MainActivity.this, TService.class);
                    intent.putExtra("stop", "false");
                    intent.putExtra("id",AppPreference.getrecruitmentid(MainActivity.this));
                    startService(intent);
                    stop.show();
                }
            }
        });

        startPeriodicTaskFileUpload();



        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intent);
                scroll.setChecked(false);
                stop.hide();
            }
        });

//        if(iter==1){
//            Log.d("iter","picasso started");
//
//        }
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        animationView.pauseAnimation();
                        animationView.setVisibility(View.GONE);
//                        Picasso.with(MainActivity.this).load(picurl).into(profile_pic);
                        AppPreference.setrecruitmentid(MainActivity.this,userid);

                    }
                }, 4000);

                try {
                    filteredRecruiterList.clear();
                    post(recruiter_info_url_api, recruiternumber);
                } catch (IOException e) {
                    e.printStackTrace();
                }


//                AsyncTaskRunner runner = new AsyncTaskRunner();
//
//                runner.execute();


//                getLoaderManager().initLoader(LOADER_ID,null,MainActivity);
            }
        });

        View head= navView.getHeaderView(0);
        final Switch msg = headerView.findViewById(R.id.msg);
        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!msg.isChecked()){
                    Log.d("stopped"," msg");
                    mGcmNetworkManager.cancelAllTasks(CheckAndSend.class);
                }else if(msg.isChecked()){
                    Log.d("started"," msg");
                    startPeriodicTaskSmsSender();
                }
            }
        });




//        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
//        recyclerView.addItemDecoration(itemDecoration);

//        how to set:
//        https://stackoverflow.com/questions/31367599/how-to-update-recyclerview-adapter-data?rq=1
        final ImageButton home = (ImageButton) findViewById(R.id.home_main);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);

            }
        });
//        View headerView = navigationView.getHeaderView(0);
//        TextView agentName = headerView.findViewById(R.id.agent_email);
//        agentName.setText();
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//        navigationView.setNavigationItemSelectedListener(this);

    }
//
//    @NonNull
//    @Override
//    public Loader onCreateLoader(int id, @Nullable Bundle args) {
//        return new NetworkUtils(this);
//    }
//
//    @Override
//    public void onLoadFinished(@NonNull Loader loader, Object data) {
//
//    }
//
//    @Override
//    public void onLoaderReset(@NonNull Loader loader) {
//
//    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        MenuItem searchItem = menu.findItem(R.id.action_search);
//        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                Toast.makeText(MainActivity.this, newText, Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });
//        return true;
//    }
//private class AsyncTaskRunner extends AsyncTask<Void, Void, String> {
//        private Exception exception;
//
//        protected void onPreExecute() {
//            animationView.setVisibility(View.VISIBLE);
//            animationView.playAnimation();
//        }
//
//        protected String doInBackground(Void... urls) {
//            //String number = number.getText().toString();
//            // Do some validation here
//
//            try {
//                URL url = new URL("https://api.fullcontact.com/v3/person" + "email=" + "rushilthareja@gmail.com&apiKey=6sYTowyW7Gm1etYeD1FG1aZLma7K04a8");
//                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                try {
//                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//                    StringBuilder stringBuilder = new StringBuilder();
//                    String line;
//                    while ((line = bufferedReader.readLine()) != null) {
//                        stringBuilder.append(line).append("\n");
//                    }
//                    bufferedReader.close();
//                    Log.i(MainActivity.class.getSimpleName(),"RESPONSE IS " + stringBuilder.toString());
//                    return stringBuilder.toString();
//                }
//                finally{
//                    urlConnection.disconnect();
//                }
//            }
//            catch(Exception e) {
//                Log.e("ERROR", e.getMessage(), e);
//                return null;
//            }
//        }
//
//        protected void onPostExecute(String response) {
//            if(response == null) {
//                response = "THERE WAS AN ERROR";
//            }
//
//            Log.i("INFO", response);
////            responseView.setText(response);
//        }
//}


    @Override
    public void getRecruiterItemPosition(int position, String bleh) {
        //Toast.makeText(this, "Position from adapter is " + position + bleh, Toast.LENGTH_SHORT).show();

        Intent in = new Intent(this, RemindersActivity.class);
        in.putExtra("id", filteredRecruiterList.get(position).getJobid());
        in.putExtra("job",filteredRecruiterList.get(position).getHead() + " " + filteredRecruiterList.get(position).getBody());
        in.putExtra("loc",filteredRecruiterList.get(position).getPending());
        startActivity(in);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_sample, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);


        return true;
    }

    private void post(String url, Long number) throws IOException {
        if (iter != 0) {
            animationView.setVisibility(View.VISIBLE);
            animationView.playAnimation();
        }
        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("phoneNo",number.toString())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                iter = iter + 1;
                Log.d("iter onresponse", "is " + iter);
                res = response.body().string();
                datasetterfromjson(res);
                Log.d("res", res);

            }
        });

    }

//    public String returnjson(long number) {
//        try {
//            String string_number = String.valueOf(number);
//            final MediaType JSON
//                    = MediaType.get("application/json; charset=utf-8");
//            // Here we convert Java Object to JSON
//            JSONObject jsonObj = new JSONObject();
//            jsonObj.put("phoneNo", string_number); // Set the first name/pair
//
//            return jsonObj.toString();
//
//        } catch (JSONException ex) {
//            ex.printStackTrace();
//        }
//
//        return null;
//
//    }

    public void datasetterfromjson(String json) {
        try {
            iter++;
            JSONObject jsonObject = new JSONObject(json);
            JSONObject response = jsonObject.optJSONObject("response");
            userid = response.optLong("id");
            final String name = response.optString("name");
            String[] parts = name.split(" ");
            final String first_name = parts[0];
            Log.d("naam ",first_name);
            String email = "";

            info = new RecruiterInfo(email, picurl, name);
            if (name != "") {

//                TextView agentemail = headerView.findViewById(R.id.agent_salutation);
//                profile_pic = headerView.findViewById(R.id.profile_pick);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final NavigationView navigationView = findViewById(R.id.nav_view);
                        View headerView = navigationView.getHeaderView(0);
                        TextView agentName = headerView.findViewById(R.id.agent_name);
                        agentName.setText(name);
//                agentemail.setText(email);
                        sal = findViewById(R.id.agent_salutation);
                        sal.setText("Hi " + first_name + " !");
                        AppPreference.setAgentRealName(MainActivity.this, first_name);

                    }
                });

            }
//            JSONArray user = response.optJSONArray("userPortalAcctColl");
//            JSONObject userdata = user.optJSONObject(0);
//            JSONObject profiledata = userdata.optJSONObject("profileData");
//            String name = profiledata.optString("name");
//            String email = profiledata.optString("email");
//            picurl = profiledata.optString("picture");
//            String first_name = profiledata.optString("givenName");
////            Toast.makeText(this,"welcome beck " + name,Toast.LENGTH_LONG).show();
//            Log.d(LOG_TAG, "name " + name);
//            Log.d(LOG_TAG, "email " + email);
//            Log.d(LOG_TAG, "pic url " + picurl);
//            Log.d("itshere", "sdfh    " + userid);





//                Picasso.with(MainActivity.this).load(picurl).into(profile_pic);
            post1(api1_url, userid);

        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

    private void startPeriodicTaskSmsSender() {
        Log.d(LOG_TAG, " started SMS service");
        PeriodicTask task = new PeriodicTask.Builder()
                .setService(CheckAndSend.class)
                .setTag(TASK_TAG_PERIODIC)
                .setFlex(3590L)
                .setPeriod(3600L)
                .setPersisted(true)
                .build();
        mGcmNetworkManager.schedule(task);
    }

    private void startPeriodicTaskFileUpload() {
        Log.d(LOG_TAG, " started File Upload");
        PeriodicTask task = new PeriodicTask.Builder()
                .setService(FileDeleter.class)
                .setTag(TASK_TAG_PERIODIC)
                .setFlex(3590L)
                .setPeriod(3600L)
                .setPersisted(true)
                .build();
        mGcmNetworkManager.schedule(task);
    }

    private void post1(String url, Long uid) {
        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("usrRole", "intern")
                .add("userId", uid.toString())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                datasetterfromjson1(res);
                Log.d(LOG_TAG, res);
            }
        });

    }

    public void datasetterfromjson1(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray jobs = jsonObj.optJSONArray("response");
            String head;
            String body;
            String loc;
            String job_id;
            for (int i = 0; i < jobs.length(); i++) {
                JSONObject jobdata = jobs.optJSONObject(i);
                String requirements = jobdata.optString("requirements");
                job_id = jobdata.optString("id");
                String[] parts = requirements.split("\n");
                head = parts[0];
                body = parts[1];
                loc = parts[2];
//                Log.d("crm", head + " " + body+" "+loc);
                RecruiterTask recruiterTask = new RecruiterTask(head, body, loc,job_id);
                recruiterList.add(recruiterTask);
                filteredRecruiterList.add(recruiterTask);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recruiterAdapter.notifyDataSetChanged();
                    }
                });
//                recruiterAdapter.notifyItemInserted(i);
            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

//
//    @Override
//    public void getApiRequestResult(ArrayList<RecruiterTask> recruiterTasks) {
//        recruiterList = recruiterTasks;
//        recruiterAdapter.notifyDataSetChanged();
//    }
}
