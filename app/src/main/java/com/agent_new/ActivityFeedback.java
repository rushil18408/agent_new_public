package com.agent_new;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import services.TService;
import utils.AppPreference;

public class ActivityFeedback extends AppCompatActivity {

    ListView lst;

    String api = "http://139.59.68.10/API/getFollowupStatusConfig.php";
    String uid, res;
    String requirementId, recruiterId;
    String LOG_TAG = "ActivityFeedback";
    ArrayList<String> options = new ArrayList<>();
    ArrayAdapter<String> arrayadapter;
    TextView quest;
    String url, question, hint, control, update_key, next;
    ImageButton call;
    String phone,hiringid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);



        phone = AppPreference.gettempcandphone(this);
        hiringid = AppPreference.gettemphiringid(this);

        quest = findViewById(R.id.feedback_question);
        call = findViewById(R.id.call_button_assg);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentu = new Intent(Intent.ACTION_DIAL);
                intentu.setData(Uri.parse("tel:" + phone));
                startActivity(intentu);
                if(!isServiceRunning(TService.class)) {
                    final Intent in = new Intent(ActivityFeedback.this, TService.class);
                    in.putExtra("stop", "true");
                    startService(in);
                }
            }
        });







        recruiterId = AppPreference.gettemprecruiterId(this);
        requirementId = AppPreference.gettemprequirementId(this);
        uid = AppPreference.gettempuid(this);




        lst = (ListView) findViewById(R.id.listview_feedback);
        arrayadapter = new ArrayAdapter<>(this, R.layout.followup_row, R.id.textview_followup, options);
        lst.setAdapter(arrayadapter);
        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                update_resp(options.get(position), url, update_key, next,position+1);
//                Intent ccc = new Intent(ActivityFeedback.this, ActivityFollow_up.class);
//                startActivity(ccc);
//                Toast.makeText(ActivityFeedback.this,position+"has been clicked",Toast.LENGTH_LONG).show();
            }
        });

        try {

            post(api, uid, recruiterId, requirementId);

        } catch (IOException e) {
            Toast.makeText(this, "COULD NOT LOAD DATA HIT BUTTON", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }




        TextView name_feed = findViewById(R.id.name_read_feedback);
        name_feed.setText(AppPreference.gettempname(this));


        ImageButton back = findViewById(R.id.back_read_feedback);
        CircleImageView profile = findViewById(R.id.readmore_profile_feedback);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chahat = new Intent(ActivityFeedback.this, ProfileActivity.class);
                startActivity(chahat);
            }
        });


    }

    private void post(String url, String uid, String recruiterId, String requirementId) throws IOException {

        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("userId", uid)
                .add("recruiterId", recruiterId)
                .add("requirementId", requirementId)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                String extraStr="something_else";
                String urli="";
                try {
                    extraStr = getIntent().getExtras().getString("response");
                    urli = getIntent().getExtras().getString("url");
                    update_key = getIntent().getExtras().getString("key");
                    next = getIntent().getExtras().getString("next");
                    Log.d("extra",extraStr);
                } catch (NullPointerException e ) {
                    extraStr = "something_else";
                    Log.d("extra",extraStr);
                }
                if(!extraStr.equals("something_else")){
                    update_resp(extraStr,urli,update_key,next,0);
                }else{
                    datasetterfromjson(res, " ");
                    Log.d("res", res);

                }

            }
        });

    }

    public void datasetterfromjson(String json, String start) {
        try {

            JSONObject jsonObj = new JSONObject(json);
            if (start == " ") {
                start = jsonObj.getString("start");
            }
            JSONObject config = jsonObj.optJSONObject("config");
            Iterator i = config.keys();
            JSONObject iter = new JSONObject();
            Log.d("key main",start);
            String found_key = "";
            boolean cont = true;
            while (i.hasNext() && cont) {
                String key = (String) i.next();
                iter = config.getJSONObject(key);
                String regexKey = iter.optString("RegexKey");
                Log.d("key",regexKey);
                if (start.matches(regexKey)) {
                    Log.d("key matched",regexKey);
                    found_key = key;
                    cont = false;
                }
            }
            JSONObject req = config.getJSONObject(found_key);
            next = req.getString("NextArr");
            if(next.equals("Done")){
                Intent ccco = new Intent(ActivityFeedback.this,AssignedcandidatesActivity.class);
                ccco.putExtra("Done","Done");
                startActivity(ccco);

                finish();
            }
            control = req.getString("controlType");
            question = req.getString("title");
            url = req.getString("updateDataUrl");
            hint = req.getString("textHint");
            update_key = req.getString("sendValueKey");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    quest.setText(question);
                }
            });
            if (control.equals("drop_down_new_page")) {
                options.clear();
                JSONObject opt = req.optJSONObject("options");

                Iterator k = opt.keys();
                while (k.hasNext()) {
                    String key = (String) k.next();
                    String val = opt.optString(key);
                    Log.d("val", val);
                    options.add(val);
                }
//            months = new String[options.size()];
//            for (int j = 0; j < options.size(); j++) {
//                months[j] = options.get(j);
//                Log.d("val", months[j]);
//            }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        arrayadapter.notifyDataSetChanged();
                        quest.setText(question);
                    }
                });

            } else if (control.equals("EditText")) {
                Intent ccc = new Intent(ActivityFeedback.this, ActivityFollow_up.class);
                ccc.putExtra("question", question);
                ccc.putExtra("head", found_key);
                ccc.putExtra("update_url", url);
                ccc.putExtra("hint", hint);
                ccc.putExtra("key", update_key);
                ccc.putExtra("next", next);
                startActivity(ccc);
            } else if (control.equals("dateSelector")) {
                Intent ccc = new Intent(ActivityFeedback.this, ActivityDate.class);
                ccc.putExtra("question", question);
                ccc.putExtra("head", found_key);
                ccc.putExtra("update_url", url);
                ccc.putExtra("hint", hint);
                ccc.putExtra("key", update_key);
                ccc.putExtra("next", next);
                startActivity(ccc);
            }


//                recruiterAdapter.notifyItemInserted(i);

        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

    public void update_resp(String response, String url, String key, String n,Integer option_number) {
        JSONObject jsonObj = new JSONObject();
        JSONObject jsonObject = new JSONObject();


        url="http://159.89.162.247:8080//walkinapp/api/hiringRequirements/"+url;

        String requirementid = AppPreference.gettemprequirementId(ActivityFeedback.this);


        if(n.contains("##")){
           n=n.replaceAll("##",option_number.toString());
        }


        try {
            jsonObject.put("id",requirementid);
            jsonObj.put("hiringRequirementsforCRM",jsonObject);
            jsonObj.put("id",hiringid);
            jsonObj.put(key, response);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("username", "yourEmail@com");
//            jsonObject.put("password", "yourPassword");
//            jsonObject.put("anyKey", "anyValue");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody body = RequestBody.create(JSON,jsonObj.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("updatehogaya",response.toString());


            }
        });
            datasetterfromjson(res, n);

        Log.i("ActivityFeedback", n);


    }
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public void send_whatsapp(String number, String text) {
        String formattedNumber;
        if (number.length() == 10) {
            formattedNumber = "91" + number;
        } else {
            formattedNumber = number;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + formattedNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send_custom_sms(String phoneNo) {
        Intent sInt = new Intent(Intent.ACTION_VIEW);
        sInt.putExtra("sms_body", " ");
        sInt.putExtra("address", phoneNo);
        sInt.setType("vnd.android-dir/mms-sms");
        startActivity(sInt);

    }

    public void send_SMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
    public void composeEmail(String[] addresses, String subject, String saman) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, saman);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}