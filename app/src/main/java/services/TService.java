package services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.agent_new.ActivityFeedback;
import com.agent_new.MainActivity;
import com.agent_new.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import utils.AppPreference;

public class TService extends Service {

    private MediaRecorder recorder;
    private File audiofile;
    private boolean recordstarted = false;

    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    public String savedNumber = "";
    private CallReceiver callReceiver;
    public String when_to_stop = "false";
    public Intent in;
    public String name = "Candidate";
    public String file_name;
    public String uid;
    public String url;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("service", "destroy");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(callReceiver);
//        unregisterReceiver(callReceiver);
    }


    private void startServiceInForeGround() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        String channelId = "com.agent_new";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("APP RUNING IN BACKGROUND HIT RED BUTTON TO STOP")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(3 /* ID of notification */, notificationBuilder.build());
        startForeground(3, notificationBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("StartService", "TService");
        uid = AppPreference.gettempuid(this);
        url = "http://159.89.162.247:8080/walkinapp/api/recruiterCallRecordsSvc/addRecruiterCallRecordEntry";
//        in=intent;
        if (intent != null) {

            when_to_stop = intent.getStringExtra("stop");
            if (when_to_stop.equals("true")) {
                name = intent.getStringExtra("name");
            }


        }

        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_OUT);
        filter.addAction(ACTION_IN);
        callReceiver = new CallReceiver();
        this.registerReceiver(callReceiver, filter);
        startServiceInForeGround();
        return START_STICKY;
    }


    private void startRecording() {
        File sampleDir = new File(Environment.getExternalStorageDirectory(), "/CallRecordings");
        if (!sampleDir.exists()) {
            sampleDir.mkdirs();
        }
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateobj = new Date();
        currentDateTimeString = df.format(dateobj);
        file_name = "";
//        Log.e("samay",AppPreference.getAgentRealName(TService.this)+"naaam");
        if (savedNumber == "") {
            file_name = AppPreference.getPhone(TService.this) + " " + currentDateTimeString + " " + AppPreference.getAgentRealName(TService.this);
        } else {
            file_name = AppPreference.getPhone(TService.this) + " " + savedNumber + " " + currentDateTimeString + " " + AppPreference.getAgentRealName(TService.this);
        }

        try {
            audiofile = File.createTempFile(file_name, ".amr", sampleDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        file_name = audiofile.getName();
        recorder = new MediaRecorder();
//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(audiofile.getAbsolutePath());
        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.start();
        recordstarted = true;
    }

    private void stopRecording() {
        if (recordstarted) {
            recorder.stop();
            recordstarted = false;
        }

    }


    public abstract class PhonecallReceiver extends BroadcastReceiver {

        //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations

        private int lastState = TelephonyManager.CALL_STATE_IDLE;
        private Date callStartTime;
        private boolean isIncoming;
        //because the passed incoming is only valid in ringing


        @Override
        public void onReceive(Context context, Intent intent) {
//        startRecording();
            //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")) {
                savedNumber = intent.getExtras().getString("android.intent.extra.PHONE_NUMBER");
            } else {
                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
                String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                int state = 0;
                if (stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    state = TelephonyManager.CALL_STATE_IDLE;
                } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    state = TelephonyManager.CALL_STATE_OFFHOOK;
                } else if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    state = TelephonyManager.CALL_STATE_RINGING;
                }


                onCallStateChanged(context, state, number);
            }
        }

        //Derived classes should override these to respond to specific events of interest
        protected abstract void onIncomingCallReceived(Context ctx, String number, Date start);

        protected abstract void onIncomingCallAnswered(Context ctx, String number, Date start);

        protected abstract void onIncomingCallEnded(Context ctx, String number, Date start, Date end);

        protected abstract void onOutgoingCallStarted(Context ctx, String number, Date start);

        protected abstract void onOutgoingCallEnded(Context ctx, String number, Date start, Date end);

        protected abstract void onMissedCall(Context ctx, String number, Date start);

        //Deals with actual events

        //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
        //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
        public void onCallStateChanged(Context context, int state, String number) {
            if (lastState == state) {
                //No change, debounce extras
                return;
            }
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    isIncoming = true;
                    callStartTime = new Date();
                    savedNumber = number;
                    onIncomingCallReceived(context, number, callStartTime);
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                    if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                        isIncoming = false;
                        callStartTime = new Date();
                        startRecording();
                        onOutgoingCallStarted(context, savedNumber, callStartTime);
                    } else {
                        isIncoming = true;
                        callStartTime = new Date();
                        startRecording();
                        onIncomingCallAnswered(context, savedNumber, callStartTime);
                    }

                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                    if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                        //Ring but no pickup-  a miss
                        onMissedCall(context, savedNumber, callStartTime);
                    } else if (isIncoming) {
                        stopRecording();
                        onIncomingCallEnded(context, savedNumber, callStartTime, new Date());
                    } else {
                        stopRecording();

                        onOutgoingCallEnded(context, savedNumber, callStartTime, new Date());
                        Log.d("when_to_stop", "begins" + when_to_stop);
                        Intent intent1 = new Intent(TService.this, ActivityFeedback.class);
//                        intent1.putExtra("phone",savedNumber);
//                        intent1.putExtra("name",name);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent1);
                        if (when_to_stop.equals("true")) {
                            stopSelf();
                        }
                    }
                    break;
            }
            lastState = state;
        }

    }

    public class CallReceiver extends PhonecallReceiver {

        @Override
        protected void onIncomingCallReceived(Context ctx, String number, Date start) {
            Log.d("onIncomingCallReceived", number + " " + start.toString());
        }

        @Override
        protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
            Log.d("onIncomingCallAnswered", number + " " + start.toString());
        }

        @Override
        protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
            long diff = end.getTime() - start.getTime();
            Long diffSeconds = diff / 1000 % 60;

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateobj = new Date();
            String currentDateTimeString = df.format(dateobj);



            AppPreference.settemprecord(TService.this,number+";"+diffSeconds+";"+currentDateTimeString+";"+uid,audiofile.getName());

//            upload(uid, file_name, start.toString(), diffSeconds.toString());

            Log.d("onIncomingCallEnded",audiofile.getName());
        }

        @Override
        protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
            Log.d("onOutgoingCallStarted", number + " " + start.toString());
        }

        @Override
        protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
            long diff = end.getTime() - start.getTime();
            Long diffSeconds = diff / 1000 % 60;

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateobj = new Date();
            String currentDateTimeString = df.format(dateobj);

            AppPreference.settemprecord(TService.this,number+";"+diffSeconds+";"+currentDateTimeString+";"+uid,audiofile.getName());

//            upload(uid, file_name, start.toString(), diffSeconds.toString());
            Log.d("onCallEnded",number+"~"+diffSeconds+"~"+currentDateTimeString+"~"+uid+"--------"+audiofile.getName() );
        }

        @Override
        protected void onMissedCall(Context ctx, String number, Date start) {
            Log.d("onMissedCall", number + " " + start.toString());
//        PostCallHandler postCallHandler = new PostCallHandler(number, "janskd" , "")
        }

    }

    public void upload(String uid, String file_name, String start_time, String duration) {
        long num = Long.parseLong(uid);
        long dur = Long.parseLong(duration);
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("recruiterID", num);
            jsonObj.put("fileDetail", file_name);
            jsonObj.put("timeCall",start_time);
            jsonObj.put("callDuration", dur);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
//            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//            OkHttpClient client = new OkHttpClient();
//            RequestBody body = RequestBody.create(JSON,jsonObj.toString());
//            Request request = new Request.Builder()
//                    .url(url)
//                    .post(body)
//                    .build();
//            client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.e("Tservice", "some error happened in okhttp");
//                Log.e("Tservice", e.toString());
//                //               Toast.makeText(this,"check internet",0).show();
//
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//            Log.d("responseaaya",response.toString());
//            }
//        });

        Log.d("jsonis",jsonObj.toString());
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody body = RequestBody.create(JSON, jsonObj.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Tservice", "some error happened in okhttp");
                Log.e("Tservice", e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("responseaaya", response.toString());
                String res = response.body().string();
                Log.d("res", res);
            }
        });
    }

}
