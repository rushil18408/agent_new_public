package com.agent_new;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import constants.IntentHelper;
import utils.AppPreference;

public class MobileAuth extends Activity implements View.OnClickListener {

    Task<Void> task;
    private EditText phoneNumber, mainEditText;
    private String userVerificationId, userEnteredPhoneNumber, generatedSixDigitOtp;
    private Button verifyOtp, skipForNow;
    private FirebaseRemoteConfig remoteConfig;
    private TextView otpSentToNumber, resendOtp;
    private RelativeLayout pagePhone, pageOtp;
    private Boolean otpByOurApi = false;
    private int PHONE_PICK = 11;
    private ProgressDialog dialog;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private FirebaseAuth mAuth;
    private GoogleApiClient apiClient;
    private SmsRetrieverClient smsRetrieverClient;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equalsIgnoreCase("otp")) {
                if (otpByOurApi && intent.hasExtra("message")) {
                    final String message = intent.getStringExtra("message");
                    if (message.contains(generatedSixDigitOtp)) {
                        fillEdit(generatedSixDigitOtp);
                        verifyOtp();
                    }
                }

            }
        }
    };

    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                apiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(),
                    PHONE_PICK, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHONE_PICK) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                String phone = credential.getId();
                phoneNumber.setText(phone);
            }
        }
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(MobileAuth.this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_auth);
        apiClient = new GoogleApiClient.Builder(this).addApi(Auth.CREDENTIALS_API).build();
        apiClient.connect();
        smsRetrieverClient = SmsRetriever.getClient(this);
        mAuth = FirebaseAuth.getInstance();
        remoteConfig = FirebaseRemoteConfig.getInstance();
        setInitialId();
        requestHint();
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                String otp = phoneAuthCredential.getSmsCode();
                if (otp != null)
                    fillEdit(otp);
                Intent i = new Intent();
                i.putExtra(IntentHelper.INTENT_OTP_MOBILE, userEnteredPhoneNumber);
                setResult(RESULT_OK, i);
                finish();
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                if (e instanceof FirebaseTooManyRequestsException) {
//                    sendOtpByOurApi();
                    pagePhone.setVisibility(View.GONE);
                    pageOtp.setVisibility(View.VISIBLE);
                    mainEditText.requestFocus();
                    hideDialog();
                    startResendTimer();
                } else {
                    hideDialog();
                    Toast.makeText(MobileAuth.this, "Invalid Phone Number", Toast.LENGTH_SHORT).show();
                    pagePhone.setVisibility(View.VISIBLE);
                    pageOtp.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                userVerificationId = s;
                pagePhone.setVisibility(View.GONE);
                pageOtp.setVisibility(View.VISIBLE);
                mainEditText.requestFocus();
                hideDialog();
                startResendTimer();
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
            }
        };
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(MobileAuth.this).unregisterReceiver(receiver);
        hideDialog();
        super.onPause();
    }

    private void fillEdit(String sixDigitOtp) {
        mainEditText.setText(String.valueOf(sixDigitOtp));
    }

    private void startResendTimer() {
        CountDownTimer cdt = new CountDownTimer(45000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (otpByOurApi) {
                    String text = "otp sent" + millisUntilFinished / 1000;
                    resendOtp.setText(text);
                } else {
                    String text = "resend in" + "  " + millisUntilFinished / 1000;
                    resendOtp.setText(text);
                }
            }

            @Override
            public void onFinish() {
                if (otpByOurApi) {
                    resendOtp.setText("otp not received");
                    skipForNow.setVisibility(View.VISIBLE);
                } else {
                    resendOtp.setEnabled(true);
                    resendOtp.setText(getResources().getString(R.string.Resend));
                    resendOtp.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                }
            }
        };
        cdt.start();
    }

    public void setInitialId() {
        phoneNumber = findViewById(R.id.mobile_auth_enter_mobile);
        Button verifyPhone = findViewById(R.id.mobile_auth_button_phone);
        verifyOtp = findViewById(R.id.mobile_auth_button_otp);
        skipForNow = findViewById(R.id.mobileauth_skipfornow_button);
        otpSentToNumber = findViewById(R.id.mobile_auth_otp_sent_to_number);
        resendOtp = findViewById(R.id.mobile_auth_resend_text);
        pagePhone = findViewById(R.id.mobileauth_mobile_page);
        pageOtp = findViewById(R.id.mobileauth_otp_page);
        phoneNumber.requestFocus();
        resendOtp.setEnabled(false);
        resendOtp.setOnClickListener(this);
        verifyPhone.setOnClickListener(this);
        verifyOtp.setOnClickListener(this);
        skipForNow.setOnClickListener(this);
        mainEditText = findViewById(R.id.et_main_otp_field);
        mainEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkForVerifyOtpButtonVisibility();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Auto-generated method stub
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (pagePhone.getVisibility() == View.GONE) {
            pageOtp.setVisibility(View.GONE);
            pagePhone.setVisibility(View.VISIBLE);
        } else {
            Intent i = new Intent();
            setResult(RESULT_CANCELED, i);
            Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.mobile_auth_button_phone:
                sendVerificationCode();
                break;
            case R.id.mobile_auth_button_otp:
                verifyOtp();
                break;
            case R.id.mobile_auth_resend_text:
                //Resend code via our api
//                sendOtpByOurApi();
                break;
            case R.id.mobileauth_skipfornow_button:
                Intent i = new Intent();
                setResult(RESULT_CANCELED, i);
                finish();
                break;
        }
    }

//    private void sendOtpByOurApi() {
//        otpByOurApi = true;
//        JSONObject object = new JSONObject();
//        JSONArray phoneNumber = new JSONArray();
//        generateSixDigitOtp();
//        String message = remoteConfig.getString("send_otp_message");
//        message = String.format(message, getString(R.string.app_name));
//        try {
//            phoneNumber.put(userEnteredPhoneNumber);
//            object.put("contactNumbers", phoneNumber);
//            object.put("smsContent", "<#> " + message + " : " + generatedSixDigitOtp + " m2WkfqVgZ5q");
//            object.put("smsType", "transactional");
//            object.put("sender", "HIRING");
//            object.put("campaign", "otp");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//         final MediaType JSON
//                = MediaType.parse("application/json; charset=utf-8");
//        OkHttpClient client = new OkHttpClient();
//        String sendingjson = object.toString();
//        RequestBody body = RequestBody.create(JSON, sendingjson);
//        Request request = new Request.Builder()
//                .url("http://139.59.67.225:8080/walkinapp/api/sms/send")
//                .post(body)
//                .build();
//        Response response = client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Toast.makeText(MobileAuth.this, "error in request " + e.getMessage(), Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                String resp = response.body().toString();
//
//            }
//        });
//
//
//
//        webserviceRawData(, object);
//        task = smsRetrieverClient.startSmsRetriever();
//        resendOtp.setTextColor(getResources().getColor(R.color.grey));
//        resendOtp.setEnabled(false);
//        startResendTimer();
//    }


    private void asfas() {

    }

    private void generateSixDigitOtp() {
        String otp = "";
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            int value = random.nextInt(9);
            otp = otp + String.valueOf(value);
        }
        generatedSixDigitOtp = otp;
    }

    public void verifyOtp() {
        showDialog(getResources().getString(R.string.verifying));
        if (TextUtils.isEmpty(mainEditText.getText().toString())) {
            hideDialog();
            Toast.makeText(this, getString(R.string.enter_otp), Toast.LENGTH_SHORT).show();
        } else {
            String finalOtp = mainEditText.getText().toString();
            if (otpByOurApi) {
                if (finalOtp.equals(generatedSixDigitOtp)) {
                    Intent i = new Intent();
                    hideDialog();
                    i.putExtra(IntentHelper.INTENT_OTP_MOBILE, userEnteredPhoneNumber);
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    hideDialog();
                    Toast.makeText(MobileAuth.this, "Incorrect OTP", Toast.LENGTH_SHORT).show();
                }
            } else {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(userVerificationId, finalOtp);
                signInWithMobileCredential(credential);
            }
        }
    }

    public void sendVerificationCode() {
        if (phoneNumber.getText() == null || TextUtils.isEmpty(phoneNumber.getText().toString())) {
            Toast.makeText(this, getString(R.string.enter_valid_otp), Toast.LENGTH_SHORT).show();
        } else {
            String userPhone = phoneNumber.getText().toString();
            userEnteredPhoneNumber = userPhone;
            userPhone = getFormattedPhoneNumber(userPhone);
            showDialog(getString(R.string.sending_otp));
            otpSentToNumber.setText(userEnteredPhoneNumber);
            AppPreference.setPhone(MobileAuth.this, userEnteredPhoneNumber);
            PhoneAuthProvider.getInstance().verifyPhoneNumber(userPhone, 30, TimeUnit.SECONDS, this, mCallback);
        }
    }

    public void signInWithMobileCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Intent i = new Intent();
                hideDialog();
                i.putExtra(IntentHelper.INTENT_OTP_MOBILE, userEnteredPhoneNumber);
                setResult(RESULT_OK, i);
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                hideDialog();
                Toast.makeText(MobileAuth.this, getString(R.string.incorrect_otp), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showDialog(String message) {
        dialog = new ProgressDialog(this);
        dialog.setMessage(message);
        dialog.setCancelable(true);
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public void checkForVerifyOtpButtonVisibility() {
        if (TextUtils.isEmpty(mainEditText.getText().toString()) || mainEditText.getText().toString().length() != 6) {
            verifyOtp.setEnabled(false);
            verifyOtp.getBackground().setColorFilter(getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_IN);
            verifyOtp.setTextColor(getResources().getColor(R.color.white));
        } else {
            verifyOtp.setEnabled(true);
            verifyOtp.getBackground().setColorFilter(getResources().getColor(R.color.design_default_color_primary), PorterDuff.Mode.SRC_IN);
            verifyOtp.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private String getFormattedPhoneNumber(String phoneNumber) {
        if (phoneNumber.length() == 10) phoneNumber = "+91" + phoneNumber;
        return phoneNumber;
    }

}