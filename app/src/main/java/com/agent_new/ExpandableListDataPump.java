package com.agent_new;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import model.RecruiterTask;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import utils.AppPreference;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class ExpandableListDataPump {
    public static String  res,walkinid;
    public static String  url="http://159.89.162.247:8080//walkinapp/api/hiresvc/addorupdatehiringrequirement";
    public static String interviewer,interviewdate,interviewlocation,interviewtime,interviewmode;
    public static String company,location,numberOfOpening;
    public static String position,ctc,minCTC,maxCTC,joiningRequirement,genderPreference,instructions;

    public static void setWalkinid(String walk){
        walkinid=walk;
    }

    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();


        update_resp(walkinid,url,"walkinId");

        List<String> interview_details = new ArrayList<String>();
        interview_details.add("interviewdate       : "+interviewdate);
        interview_details.add("interviewlocation   : "+interviewlocation);
        interview_details.add("interviewtime       : "+interviewtime);
        interview_details.add("interviewmode       : "+interviewmode);
        interview_details.add("interviewer         : "+interviewer);

        List<String> company_details = new ArrayList<String>();
        company_details.add("company         : "+company);
        company_details.add("location        : "+location);
        company_details.add("numberOfOpening : "+numberOfOpening);

        List<String> job_details = new ArrayList<String>();
        job_details.add("position           : "+position);
        job_details.add("ctc                : "+ctc);
        job_details.add("minCTC             : "+minCTC);
        job_details.add("maxCTC             : "+maxCTC);
        job_details.add("joiningRequirement : "+joiningRequirement);
        job_details.add("instructions       : "+instructions);
        job_details.add("genderPreference   : "+genderPreference);

        expandableListDetail.put("Interview Details;"+interviewmode, interview_details);
        expandableListDetail.put("Company Details;"+company, company_details);
        expandableListDetail.put("Job Details;"+position+" at "+location, job_details);
        return expandableListDetail;




    }
    public static void update_resp(String response, String url, String key) {
        JSONObject jsonObj = new JSONObject();




        try {
            jsonObj.put(key, response);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("username", "yourEmail@com");
//            jsonObject.put("password", "yourPassword");
//            jsonObject.put("anyKey", "anyValue");
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody body = RequestBody.create(JSON,jsonObj.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("list data dump", "some error happened in okhttp");
                Log.e("list data dump", e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                datasetterfromjson(res);
                Log.d("data dump", res);


            }
        });


    }
    public static void  datasetterfromjson(String json) {
        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONObject response = jsonObj.optJSONObject("response");
            interviewer = response.optString("interViewer");
            if(interviewer.contains("<br>")){
                interviewer = interviewer.replaceAll("<br>","\n");
            }
            if(interviewer=="null" || interviewer== ""){
                interviewer="-";
            }
            interviewdate = response.optString("interviewDate");
            if(interviewdate.contains("<br>")){
                interviewdate = interviewdate.replaceAll("<br>","\n");
            }
            if(interviewdate=="null" || interviewdate== ""){
                interviewdate="-";
            }
            interviewlocation = response.optString("interviewLocation");
            if(interviewlocation.contains("<br>")){
                interviewlocation = interviewlocation.replaceAll("<br>","\n");
            }
            if(interviewlocation=="null" || interviewlocation== ""){
                interviewlocation="-";
            }
            interviewmode = response.optString("interviewProcess");
            if(interviewmode.contains("<br>")){
                interviewmode = interviewmode.replaceAll("<br>","\n");
            }
            if(interviewmode=="null" || interviewmode== ""){
                interviewmode="-";
            }
            interviewtime = response.optString("interviewTime");
            if(interviewtime.contains("<br>")){
                interviewtime = interviewtime.replaceAll("<br>","\n");
            }
            if(interviewtime=="null" || interviewtime== ""){
                interviewtime="-";
            }

            company = response.optString("client");
            if(company.contains("<br>")){
                company = company.replaceAll("<br>","\n");
            }
            if(company=="null" || company== ""){
                company="-";
            }

            location = response.optString("location");
            if(location.contains("<br>")){
                location = location.replaceAll("<br>","\n");
            }
            if(location=="null" || location== ""){
                location="-";
            }
            numberOfOpening = response.optString("numberOfOpening");
            if(numberOfOpening.contains("<br>")){
                numberOfOpening = numberOfOpening.replaceAll("<br>","\n");
            }
            if(numberOfOpening=="null" || numberOfOpening== ""){
                numberOfOpening="-";
            }
            position = response.optString("position");
            if(position.contains("<br>")){
                position = position.replaceAll("<br>","\n");
            }
            if(position=="null" || position== ""){
                position="-";
            }
            ctc = response.optString("ctc");
            if(ctc.contains("<br>")){
                ctc = ctc.replaceAll("<br>","\n");
            }
            if(ctc=="null" || ctc== ""){
                ctc="-";
            }
            minCTC = response.optString("minCTC");
            if(minCTC.contains("<br>")){
                minCTC = minCTC.replaceAll("<br>","\n");
            }
            if(minCTC=="null" || minCTC== ""){
                minCTC="-";
            }
            maxCTC = response.optString("maxCTC");
            if(maxCTC.contains("<br>")){
                maxCTC = maxCTC.replaceAll("<br>","\n");
            }
            if(maxCTC=="null" || maxCTC== ""){
                maxCTC="-";
            }
            joiningRequirement = response.optString("joiningRequirement");
            if(joiningRequirement.contains("<br>")){
                joiningRequirement = joiningRequirement.replaceAll("<br>","\n");
            }
            if(joiningRequirement=="null" || joiningRequirement== ""){
                joiningRequirement="-";
            }
            genderPreference = response.optString("genderPreference");
            if(genderPreference.contains("<br>")){
                genderPreference = genderPreference.replaceAll("<br>","\n");
            }
            if(genderPreference=="null" || genderPreference== ""){
                genderPreference="-";
            }



            instructions = response.optString("instructions");
            if(instructions.contains("<br>")){
                instructions = instructions.replaceAll("<br>","\n");
            }
            if(instructions=="null" || instructions== ""){
                instructions="-";
            }
            if(instructions.contains("&")){
                instructions = instructions.replaceAll("&"," ");
            }


        } catch (JSONException e) {
            Log.d("data dump", "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

}
