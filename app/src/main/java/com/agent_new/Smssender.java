package com.agent_new;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import adapters.Adapter_msg;
import model.SmsTask;
import services.TService;

public class Smssender extends AppCompatActivity {
    public Adapter_msg adapter;

    public ArrayList<SmsTask> list = new ArrayList<>();
    public String number="9898989898";
    public String name="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

        number = getIntent().getStringExtra("phone");
        name = getIntent().getStringExtra("name");

        RecyclerView recyclerView = findViewById(R.id.rv_main);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        SmsTask smsTask1 = new SmsTask("message string will be here");
        list.add(smsTask1);
        SmsTask smsTask2 = new SmsTask("message string will be here");
        list.add(smsTask2);
        SmsTask smsTask3 = new SmsTask("message string will be here");
        list.add(smsTask3);
        SmsTask smsTask4 = new SmsTask("message string will be here");
        list.add(smsTask4);
        SmsTask smsTask5 = new SmsTask("message string will be here");
        list.add(smsTask5);

        adapter = new Adapter_msg(this,list, (RecruiterPositionInterface) this);
        recyclerView.setAdapter(adapter);

        Button cancel = (Button) findViewById(R.id.cancel);
        Button send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                sendSMS(number);
                Toast.makeText(Smssender.this,"SMS sent to " +name +" at "+ number,Toast.LENGTH_LONG).show();
                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }
    private void sendSMS(String sendToNumber) {
        if (sendToNumber != null) {
            String message = utils.AppPreference.getSmsText(this);
            if (message == null || message.isEmpty()) {
                message = "I am busy right now and can't talk. Please contact me on WhatsApp" + " - " + "https://wa.me/918130138055";
            }
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(sendToNumber, null, message, null, null);
        } else {
            Log.e("Smssender", "Wrong number has been entered ");
        }
    }
}
