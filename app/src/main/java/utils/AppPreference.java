package utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import constants.StringUtilLocal;


/**
 * Created by Himanshu on 23-12-2016.
 */
public class AppPreference {
    private static SharedPreferences mPrefs;
    private static SharedPreferences.Editor mPrefsEditor;
    private static final String AGENT_UNIQUE_NAME = "agentName";
    private static final String AGENT_PASSWORD = "agentPassword";
    private static final String AGENT_ID = "agentId";
    private static final String AGENT_SOURCE = "agentSource";
    private static final String AGENT_POLLING_INTERVAL = "agentPollingInterval";
    private static final String IS_AGENT_REGISTERED = "isAgentRegistered";
    private static final String IS_AGENT_VERIFIED = "isAgentVerified";

    public static boolean getIsTorrEnabled() {
        return false;
    }

    public static String getAgentName(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(AGENT_UNIQUE_NAME, "UNKNOWN_AGENT");
    }

    public static void setAgentName(Context ctx, String agentName) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(AGENT_UNIQUE_NAME, agentName);
        mPrefsEditor.apply();
    }


    public static String getAgentPassword(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(AGENT_PASSWORD, StringUtilLocal.EMPTY);
    }

    public static void setAgentPassword(Context ctx, String agentPassword) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(AGENT_PASSWORD, agentPassword);
        mPrefsEditor.apply();
    }


    public static Boolean isAgentRegistered(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getBoolean(IS_AGENT_REGISTERED, false);
    }

    public static void setIsAgentRegistered(Context ctx, Boolean isAgentRegistered) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putBoolean(IS_AGENT_REGISTERED, isAgentRegistered);
        mPrefsEditor.apply();
    }

    public static String getAgentId(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(AGENT_ID, StringUtilLocal.EMPTY);
    }

    public static void setAgentId(Context ctx, String agentId) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(AGENT_ID, agentId);
        mPrefsEditor.apply();
    }

    public static String getAgentSource(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(AGENT_SOURCE, StringUtilLocal.EMPTY);
    }

    public static void setAgentSource(Context ctx, String agentSource) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(AGENT_SOURCE, agentSource);
        mPrefsEditor.apply();
    }

    public static Long getAgentPollingInterval(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getLong(AGENT_POLLING_INTERVAL, 5000);
    }

    public static void setAgentPollingInterval(Context ctx, Long pollingInterval) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putLong(AGENT_POLLING_INTERVAL, pollingInterval);
        mPrefsEditor.apply();
    }


    public static Boolean isAgentVerified(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getBoolean(IS_AGENT_VERIFIED, false);
    }

    public static void setIsAgentVerified(Context ctx, Boolean isAgentVerified) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putBoolean(IS_AGENT_VERIFIED, isAgentVerified);
        mPrefsEditor.apply();
    }

    public static void setFCMKey(Context ctx, String gcmkey) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("fcmkey", gcmkey);
        mPrefsEditor.apply();
    }

    public static String getFCMKey(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("fcmkey", StringUtilLocal.EMPTY);
    }

    public static void setUserEmailId(Context ctx, String emailId) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("email", emailId);
        mPrefsEditor.apply();
    }

    public static String getUserEmailId(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("email", StringUtilLocal.EMPTY);
    }

    public static void setSmsText(Context ctx, String smsText) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("message", smsText);
        mPrefsEditor.apply();
    }

    public static String getSmsText(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("message", "");
    }

    public static void setPhone(Context ctx, String phoneNumber) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("phoneNo", phoneNumber);
        mPrefsEditor.apply();
    }

    public static String getPhone(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("phoneNo", StringUtilLocal.EMPTY);
    }
    public static void setcounter(Context ctx, int count) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putInt("count",count);
        mPrefsEditor.apply();
    }
    public static int getcounter(Context ctx) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getInt("count",0);
    }
    public static int getiter(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getInt("iter",0);
    }

    public static void setIter(Context ctx,int iterval){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putInt("iter",iterval);
        mPrefsEditor.apply();
    }

    public static String getAgentRealName(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("name","");
    }

    public static void setAgentRealName(Context ctx,String naam){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("name",naam);
        mPrefsEditor.apply();
    }
    public static Long getrecruitmentid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getLong("rid",15235931);
    }

    public static void setrecruitmentid(Context ctx,Long rid){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putLong("rid",rid);
        mPrefsEditor.apply();
    }
    public static String getcomment(Context ctx,String Candidate_name,String type){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(Candidate_name+type," ");
    }

    public static void setcomment(Context ctx,String Candidate_name,String type,String Comment){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(Candidate_name+type,Comment);
        mPrefsEditor.apply();
    }
    public static String gettempuid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_uid","1234");
    }

    public static void settempuid(Context ctx,String Candidate_uid){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_uid",Candidate_uid);
        mPrefsEditor.apply();
    }
    public static void removeuid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_uid").commit();
    }
    public static String gettempname(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_name_temp"," ");
    }

    public static void settempname(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_name_temp",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removename(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_name_temp").commit();
    }

    public static String gettemprecruiterId(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_recruiterId"," ");
    }

    public static void settemprecruiterId(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_recruiterId",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removerecruiterId(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_recruiterId").commit();
    }
    public static String gettemprequirementId(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_requirementId"," ");
    }

    public static void settemprequirementId(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_requirementId",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removerequirementId(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_requirementId").commit();
    }
    public static String gettempcandphone(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_temp_phone","8851505304");
    }

    public static void settempcandphone(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_temp_phone",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removetempphone(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_temp_phone").commit();
    }
    public static String gettempwalkinid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_walkinid","13904789");
    }

    public static void settempwalkinid(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_walkinid",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removetempwalkinid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_walkinid").commit();
    }
    public static String gettemphiringid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString("candidate_hiringid","10537");
    }

    public static void settemphiringid(Context ctx,String Candidate_name){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString("candidate_hiringid",Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removetemphiringid(Context ctx){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove("candidate_hiringid").commit();
    }
    public static String gettemprecord(Context ctx,String filename){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        return mPrefs.getString(filename,"1-2-3-4");
    }

    public static void settemprecord(Context ctx,String Candidate_name,String filename){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(filename,Candidate_name);
        mPrefsEditor.apply();
    }
    public static void removetemprecord(Context ctx,String filename){
        mPrefs = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.remove(filename).commit();
    }




}
