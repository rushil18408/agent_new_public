package model;

public class RecruiterInfo {

    private String mEmail;
    private String mPictureUrl;
    private String mName;

    public RecruiterInfo(String email, String pictureUrl, String name) {
        mEmail = email;
        mPictureUrl = pictureUrl;
        mName = name;
    }

    public String getmEmail() {
        return mEmail;
    }

    public String getmPictureUrl() {
        return mPictureUrl;
    }

    public String getmName() {
        return mName;
    }

}