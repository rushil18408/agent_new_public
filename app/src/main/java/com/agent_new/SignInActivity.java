package com.agent_new;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Calendar;
import java.util.Date;

import constants.IntentHelper;
import utils.AppPreference;


/**
 * imported skillmao made by rushil
 */
public class SignInActivity extends Activity {
    private static final int REQUEST_ALL = 45;
    public static String TAG = "SignInActivity";
    public static int RC_NUMBER_SIGN_IN = 7;
    int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    private SignInButton loginbutton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    // ...463536885434-4n83eie39papki105gr4pk767o2pi9gl.apps.googleusercontent.com
    // Initialize Firebase Auth


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        loginbutton = findViewById(R.id.btn_login);
        loginbutton.setEnabled(true);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken("463536885434-4n83eie39papki105gr4pk767o2pi9gl.apps.googleusercontent.com")
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SignInActivity.this, "login initiated" , Toast.LENGTH_SHORT).show();
                signIn();
            }
        });
        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
//this is where we start the Auth state Listener to listen for whether the user is signed in or not
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
// Get signedIn user
                FirebaseUser user = firebaseAuth.getCurrentUser();
//if user is signed in, we call a helper method to save the user details to Firebase
                if (user != null) {
// User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                    handleSignInResult();


                } else {
// User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                String idToken = account.getIdToken();
                String email = account.getEmail();
                Log.d("ruhil", "id  " + idToken + "email  " + email);
                AppPreference.setUserEmailId(this, email);
                //AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
                firebaseAuthWithGoogle(account, idToken);
            } else {
                Toast.makeText(this, "GOOGLE SIGN FAILED ", Toast.LENGTH_LONG).show();
            }

            //            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            try {
//
//                // Google Sign In was successful, authenticate with Firebase
//                GoogleSignInAccount account = task.getResult(ApiException.class);
//                firebaseAuthWithGoogle(account);
//                handleSignInResult(task);
//            } catch (ApiException e) {
//                // Google Sign In failed, update UI appropriately
//                Log.w(TAG, "Google sign in failed", e);
//                // ...
//            }

        } else if (requestCode == RC_NUMBER_SIGN_IN) {
            if (resultCode == RESULT_OK && data != null) {
                String phoneNumber = data.getStringExtra(IntentHelper.INTENT_OTP_MOBILE);
                AppPreference.setPhone(this, phoneNumber);
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "PLEASE ENTER THE NUMBER", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleSignInResult() {


        Intent mobileIntent = new Intent(this, MobileAuth.class);
        startActivityForResult(mobileIntent, RC_NUMBER_SIGN_IN);
        Date c = Calendar.getInstance().getTime();

    }

    @Override
    protected void onStart() {
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (account != null && currentUser != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        super.onStart();
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct, String idtoken) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + idtoken);

        AuthCredential credential = GoogleAuthProvider.getCredential(idtoken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            handleSignInResult();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            // Toast.makeText(this,"firebase authentication has failed ",Toast.LENGTH_LONG).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }


}

