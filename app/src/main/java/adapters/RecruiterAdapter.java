package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agent_new.R;
import com.agent_new.RecruiterPositionInterface;

import java.util.ArrayList;

import model.RecruiterTask;

public class RecruiterAdapter extends RecyclerView.Adapter<RecruiterAdapter.RecruiterHolder>{

    private Context mContext;
    private ArrayList<RecruiterTask> mRecruiterList;
//    private ArrayList<RecruiterTask> mFileteredRecruiterList;
    private RecruiterPositionInterface mPositionInterface;

    public RecruiterAdapter(Context context, ArrayList<RecruiterTask> recruiterList,RecruiterPositionInterface positionInterface) {
        mContext = context;
        mRecruiterList = recruiterList;
//        mFileteredRecruiterList=recruiterList;
        mPositionInterface = positionInterface;
    }

    @NonNull
    @Override
    public RecruiterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_row,parent,false);
        return new RecruiterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecruiterHolder holder, int position) {
        final RecruiterTask recruiterTask = mRecruiterList.get(position);
        holder.body.setText(recruiterTask.body);
        holder.head.setText(recruiterTask.head);
        holder.pending.setText(recruiterTask.pending);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPositionInterface.getRecruiterItemPosition(holder.getAdapterPosition(),"LOL");
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mRecruiterList != null) {
            return mRecruiterList.size();
        } else {
            return 0;
        }
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                String query = constraint.toString();
//                ArrayList<RecruiterTask> Filetered = new ArrayList<>();
//
//                if (query.isEmpty()) {
//                    Filetered = mRecruiterList;
//                } else {
//                    for (RecruiterTask job : mRecruiterList) {
//                        if(job.getHead().toLowerCase().startsWith(query.toLowerCase())){
//                            Log.d("query",query.toLowerCase()+"checked in "+job.getHead().toLowerCase());
//                            Filetered.add(job);
//                        }
//                    }
//                }
//                FilterResults results = new FilterResults();
//                results.count = Filetered.size();
//                results.values = Filetered;
//                return results;
//            }
//
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//                mFileteredRecruiterList = (ArrayList<RecruiterTask>) results.values;
////                for (int i=0;i<mFileteredRecruiterList.size()-1;i++)
////                {
////                    Log.d("pehla",mFileteredRecruiterList.get(i).getHead());
////                }
//                notifyDataSetChanged();
//            }
//        };
//    }


    class RecruiterHolder extends RecyclerView.ViewHolder {

            final TextView head;
            final TextView body;
            final TextView pending;

            RecruiterHolder(View itemView) {
                super(itemView);
                head = itemView.findViewById(R.id.msg);
                body = itemView.findViewById(R.id.interview);
                pending = itemView.findViewById(R.id.location);
//                Typeface typeface = Typeface.createFromFile();
//                pending.setTypeface(typeface);
            }
        }





}
