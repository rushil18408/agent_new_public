package com.agent_new;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class AgentApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
    }

}