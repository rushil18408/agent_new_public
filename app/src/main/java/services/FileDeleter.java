package services;


import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;

import com.agent_new.MainActivity;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import utils.AppPreference;

public class FileDeleter extends GcmTaskService {

    private static final String LOG_TAG = FileDeleter.class.getSimpleName();
    public int tag = 0;
    public String url = "http://159.89.162.247:8080/walkinapp/api/recruiterCallRecordsSvc/addRecruiterCallRecordEntry";

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        doPeriodicTask();
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.d(LOG_TAG, "onRunTask: " + taskParams.getTag());
        String tag = taskParams.getTag();
        int result = GcmNetworkManager.RESULT_SUCCESS;
        if (MainActivity.TASK_TAG_PERIODIC.equals(tag)) {
            result = doPeriodicTask();
        }
        return result;
    }

    private int doPeriodicTask() {
        boolean t;
        Log.e("upload","initiated");
        String filelocationinitial = Environment.getExternalStorageDirectory().getPath();
        String filelocationfinal = filelocationinitial + "/CallRecordings";
        File dir = new File(filelocationfinal);
        if (dir.length() != 0){
            for (File file : dir.listFiles()) {
                if (file.length() < 1024) {
                    t = file.delete();
                    if (t) {
                        Log.d(LOG_TAG, " DELETED A SMALL AUDIO FILE size -> " + file.length());
                    } else {
                        Log.d(LOG_TAG, " FILE DELETION FAILED ");
                    }

                } else {
                    int count = 0;
//                    while(tag!=1 && count < 5){
//                        count++;
//                        upload(file);
//                        Log.d(LOG_TAG,"TRIED UPLOADING FOR THESE MANY TIMES ->" + count);
//                    }
                    upload(file);
                    Log.d(LOG_TAG, " FILE TOO BIG TO DELETE SO NOT DELETED size -> " + file.length());
                }


            }
        }
        else{
            Log.d(LOG_TAG, " AUDIORECORDER DIRECTORY IS EMPTY");
        }

        return GcmNetworkManager.RESULT_SUCCESS;
    }
//    private int doPeriodicTask(){
//        String filelocationinitial = Environment.getExternalStorageDirectory().getPath();
//        String filelocationfinal = filelocationinitial + "/DCIM/Camera/VID20190527163640.mp4";
//        File file = new File(filelocationfinal);
//        upload(file);
//        return GcmNetworkManager.RESULT_SUCCESS;
//    }
    public void upload(final File file){
        final String name = file.getName();
        Log.d("partis",name);
        StorageReference mStorageRef;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        Uri f = Uri.fromFile(file);
        String agname = AppPreference.getAgentRealName(FileDeleter.this);
        String path="";
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);
        if(agname!="") {
            path = "voiceRecordings/" + agname +"/"+formattedDate+"/"+file.getName();
        }
        else{
            path = "voiceRecordings/" + file.getName();
        }
        final StorageReference riversRef = mStorageRef.child(path);

        riversRef.putFile(f)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                // getting image uri and converting into string
                                Uri downloadUrl = uri;
                                String fileUrl = downloadUrl.toString();
                                String respo =   AppPreference.gettemprecord(FileDeleter.this,name);
                                String batein[] = respo.split(";");
//                                Log.d("part is",file.getName().replaceAll(" ","-"));
                                for (String a : batein)
                                    Log.d("partis",a+name);

                                upload_data(batein[3],name,batein[2],batein[1],downloadUrl.toString());

                                Log.d("uploader","THE UPLOAD URI IS -> " + fileUrl);
                                AppPreference.removetemprecord(FileDeleter.this,name);
                                file.delete();
                                tag = 1;


                            }
                        });

//                        Uri downloadUri = taskSnapshot.getUploadSessionUri();
//                        Task<Uri> down = riversRef.getDownloadUrl();
//                        Log.d("down",down.toString());
//                        long data = taskSnapshot.getBytesTransferred();
////                        boolean delete = file.delete();
//                        Log.d(LOG_TAG,"SUCCESSFULY UPLOADED THIS MANY BYTES -> " + data);
//                        Log.d(LOG_TAG,"THE UPLOAD URI IS -> " + downloadUri.toString());
//                        Log.d(LOG_TAG,"FILE DELETION GAVE - > " + delete);


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        // ...
                        String respo =   AppPreference.gettemprecord(FileDeleter.this,name);
                        String batein[] = respo.split("~");
                        upload_data(batein[3],name,batein[2],batein[1],"UPLOAD FAILED");
                        AppPreference.removetemprecord(FileDeleter.this,name);
                        Log.d(LOG_TAG,"UPLOAD WAS UNSUCCESSFULL" );
                        tag = 0;
                    }
                });
    }
    public void upload_data(String uid, String file_name, String start_time, String duration,String urli) {
        long num = Long.parseLong(uid);
        long dur = Long.parseLong(duration);
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("recruiterID", num);
            jsonObj.put("fileDetail", urli);
            jsonObj.put("timeCall",start_time);
            jsonObj.put("callDuration", dur);
            jsonObj.put("file_name",file_name);
//            jsonObj.put("file_name",file_name);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
//            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//            OkHttpClient client = new OkHttpClient();
//            RequestBody body = RequestBody.create(JSON,jsonObj.toString());
//            Request request = new Request.Builder()
//                    .url(url)
//                    .post(body)
//                    .build();
//            client.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.e("Tservice", "some error happened in okhttp");
//                Log.e("Tservice", e.toString());
//                //               Toast.makeText(this,"check internet",0).show();
//
//
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//            Log.d("responseaaya",response.toString());
//            }
//        });

        Log.d("jsonis",jsonObj.toString());
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // put your json here
        RequestBody body = RequestBody.create(JSON, jsonObj.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("Tservice", "some error happened in okhttp");
                Log.e("Tservice", e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("responseaaya", response.toString());
                String res = response.body().string();
                Log.d("res", res);
            }
        });
    }



}