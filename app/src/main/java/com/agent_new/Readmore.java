package com.agent_new;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import constants.IntentHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import services.TService;
import utils.AppPreference;

public class Readmore extends AppCompatActivity {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    String email_receiver = "rushilthareja@gmail.com";
    String email_subject = "DONT WORRY IT ONLY FOR TESTING";
    String body = "aur batao kya hal chal hain ? sab badiya ?";
    String resp[] = new String[1];
    String whatsapp_number = "9354464293";
    String LOG_TAG = "activity_readmore";
    String whatsapp_content;
    String body_share = "head";
    String body_subject = "body";
    String botlink = "https://wa.me/918130138055";
    String company_and_job = "company and job";
    String interview_details = "interview details";
    private View popupContentView, popupContentView2;
    private PopupWindow popupWindow, popupwindowmail;
    private View main;
    private ImageButton whatsapp_button, sms_button,goback;
    private View bottombar, bottomconstraint;
    private String botlink_subject_mail = "subject for botlink";
    private String company_job_subject_mail = "company and job details";
    private String interview_confirmation_subject_mail = "confirmation mail";
    private String customise_subject_mail = " ";
    private String candidate_name, candidate_phone, candidate_uid,walkinid;
    private TextView naam;
    private EditText job_comments;
    private String subb=" ";
    String old;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readmore);

        whatsapp_number=AppPreference.gettempcandphone(this);

        try {
            post("http://159.89.162.247:8080/walkinapp/api/usersvc/getUserDetailByContactNo",whatsapp_number);

        } catch (IOException e) {
            Toast.makeText(this, "COULD NOT LOAD EMAIL OPEN PAGE AGAIN TO SEND EMAIL", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }





        candidate_name = getIntent().getStringExtra("name");

        candidate_phone = getIntent().getStringExtra("phone");

        candidate_uid = getIntent().getStringExtra("uid");

        Log.d("job comments",AppPreference.getcomment(this,candidate_uid, IntentHelper.JOB_COMMENT)+" AND CURRENT UID"+ AppPreference.gettempuid(this));

        setvalues(candidate_phone, candidate_name);

        job_comments = findViewById(R.id.comments_readmore);
        job_comments.setHint(AppPreference.getcomment(Readmore.this, candidate_uid, IntentHelper.JOB_COMMENT));

        old = AppPreference.getcomment(Readmore.this, candidate_uid, IntentHelper.JOB_COMMENT);
        old=old+" ";
        job_comments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String newer = s.toString();
                String finals;
                finals = old + newer;
                AppPreference.setcomment(Readmore.this, candidate_uid, IntentHelper.JOB_COMMENT, finals);
                job_comments.setHint(finals);
                job_comments.clearFocus();
                Log.i(LOG_TAG, "FINAL COMMENT IS " + old);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        walkinid = AppPreference.gettempwalkinid(this);
        ExpandableListDataPump expandableListDataPump = new ExpandableListDataPump();
        expandableListDataPump.setWalkinid(walkinid);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expandableListDetail = expandableListDataPump.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);

        bottombar = findViewById(R.id.bottombar);
        bottomconstraint = findViewById(R.id.bottomconstraint);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });

        goback = findViewById(R.id.back_read);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        ImageButton call =  findViewById(R.id.call_button_readmore);


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentu = new Intent(Intent.ACTION_DIAL);
                intentu.setData(Uri.parse("tel:" + candidate_phone));
                startActivity(intentu);
                if(!isServiceRunning(TService.class)) {
                    final Intent in = new Intent(Readmore.this, TService.class);
                    in.putExtra("stop", "true");
                    in.putExtra("id",AppPreference.getrecruitmentid(Readmore.this));
                    startService(in);
                }

            }
        });

        ImageButton addresp = findViewById(R.id.addresponses_readmore);
        addresp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ch = new Intent(Readmore.this, ActivityFeedback.class);
                ch.putExtra("uid",candidate_uid);
                startActivity(ch);
            }
        });

        ImageView addresparrow = findViewById(R.id.addresponses_readmore_arrow);
        addresparrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cho = new Intent(Readmore.this, ActivityFeedback.class);
                startActivity(cho);
            }
        });

        CircleImageView profile = findViewById(R.id.readmore_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chahat = new Intent(Readmore.this, ProfileActivity.class);
                startActivity(chahat);
            }
        });
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupContentView = layoutInflater.inflate(R.layout.message_popup, null);
        popupWindow = new PopupWindow(Readmore.this);
        popupWindow.setContentView(popupContentView);
        popupWindow.setWidth((int) (width * 0.7));
        popupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(null);

        LayoutInflater layouter = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popupContentView2 = layouter.inflate(R.layout.message_pop_readmore, null);
        popupwindowmail = new PopupWindow(Readmore.this);
        popupwindowmail.setContentView(popupContentView2);
        popupwindowmail.setWidth((int) (width * 0.7));
        popupwindowmail.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popupwindowmail.setFocusable(true);
        popupwindowmail.setBackgroundDrawable(null);

        main = findViewById(R.id.main_layout_readmore);
        ImageButton img_button = findViewById(R.id.mail_readmore);
        img_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupwindowmail.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                resp[0] = email_receiver;
                Button option_botlink = popupContentView2.findViewById(R.id.Share_Bot_link_with_Candidate_mail);
                Button option_details = popupContentView2.findViewById(R.id.Share_Company_and_Job_Details_mail);
                Button option_interview = popupContentView2.findViewById(R.id.Share_Interview_Details_mail);
                Button option_Customise = popupContentView2.findViewById(R.id.Customise_Message_mail);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        whatsapp_content = exctract_data(expandableListDetail);
                        popupWindow.dismiss();
                        composeEmail(resp,subb, whatsapp_content);

                    }

                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        composeEmail(resp,"click on link", whatsapp_content);

                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = exctract_data_interview(expandableListDetail);
                        popupWindow.dismiss();
                        composeEmail(resp,subb, whatsapp_content);

                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        composeEmail(resp,subb, whatsapp_content);


                    }
                });


            }
        });
        whatsapp_button = findViewById(R.id.whatsapp_readmore);
        whatsapp_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                Button option_botlink = popupContentView.findViewById(R.id.Share_Bot_link_with_Candidate);
                Button option_details = popupContentView.findViewById(R.id.Share_Company_and_Job_Details);
                Button option_interview = popupContentView.findViewById(R.id.Share_Interview_Details);
                Button option_Customise = popupContentView.findViewById(R.id.Customise_Message);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = exctract_data(expandableListDetail);
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = exctract_data_interview(expandableListDetail);
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);
                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        send_whatsapp(whatsapp_number, whatsapp_content);

                    }
                });

            }
        });
        sms_button = findViewById(R.id.message_readmore);
        sms_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.showAtLocation(main, Gravity.BOTTOM, 0, 350);
                whatsapp_content = " ";
                Button option_botlink = popupContentView.findViewById(R.id.Share_Bot_link_with_Candidate);
                Button option_details = popupContentView.findViewById(R.id.Share_Company_and_Job_Details);
                Button option_interview = popupContentView.findViewById(R.id.Share_Interview_Details);
                Button option_Customise = popupContentView.findViewById(R.id.Customise_Message);
                option_details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = exctract_data(expandableListDetail);
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_botlink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = botlink;
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_interview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = exctract_data_interview(expandableListDetail);
                        popupWindow.dismiss();
                        send_SMS(whatsapp_number, whatsapp_content);
                    }
                });
                option_Customise.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        whatsapp_content = " ";
                        popupWindow.dismiss();
                        send_custom_sms(whatsapp_number);

                    }
                });


//                send_SMS(whatsapp_number,whatsapp_content);
            }
        });


    }

    public void setvalues(String phone, String name) {
        naam = findViewById(R.id.name_read);
        naam.setText(name);
    }

    public void composeEmail(String[] addresses, String subject, String saman) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, saman);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void send_whatsapp(String number, String text) {
        String formattedNumber;
        if (number.length() == 10) {
            formattedNumber = "91" + number;
        } else {
            formattedNumber = number;
        }

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + formattedNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void send_custom_sms(String phoneNo) {
        Intent sInt = new Intent(Intent.ACTION_VIEW);
        sInt.putExtra("sms_body", " ");
        sInt.putExtra("address", phoneNo);
        sInt.setType("vnd.android-dir/mms-sms");
        startActivity(sInt);

    }

    public void send_SMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent", Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public String exctract_data_interview(HashMap<String, List<String>> expandableListDetail){
        List<String> inter;
        inter = new ArrayList<>();
        String bhej="";
        String response="";
        for ( String key : expandableListDetail.keySet() ) {
            if(key.contains("Interview Details")){
               inter = expandableListDetail.get(key);
               bhej = key;
               subb=key;
            }
        }
        response = create(inter,bhej);
        return(response);
    }
    public String exctract_data(HashMap<String, List<String>> expandableListDetail){
        List<String> inter;
        inter = new ArrayList<>();
        List<String> inter2;
        inter2 = new ArrayList<>();
        String bhej="";
        String response="";
        for ( String key : expandableListDetail.keySet() ) {
            if(key.contains("Company Details")){
                inter = expandableListDetail.get(key);
                bhej = bhej+key;
                subb=subb+"\n"+key;
            }
            if(key.contains("Job Details")){
                inter2 = expandableListDetail.get(key);
                bhej = bhej+key;
                subb=subb+"\n"+key;
            }

        }
        subb=subb.replaceAll(";","\n");
        inter2.addAll(inter);
        response = create(inter2,bhej);
        return(response);
    }
    public String create(List<String> list,String Title){
        String finals=" ";
        Title = Title.replaceAll(";","\n");
        finals=finals.concat("\n"+Title.toUpperCase()+"\n");
        for(int i=0;i<list.size();i++){
            finals=finals+list.get(i)+"\n";
        }
        return(finals);
    }
    private void post(String url, String number) throws IOException {

        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("phoneNo", number)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                datasetterfromjson(res);
                Log.d("data from api 1", res);


            }
        });

    }

    public void datasetterfromjson(String json) {
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject response = jsonObject.optJSONObject("response");
            String mail=" ";

            JSONArray email = response.optJSONArray("userEmailColl");
            for (int i = 0; i < email.length(); i++){
                JSONObject em = email.getJSONObject(i);
                mail = em.optString("email");

            }
            email_receiver = mail;



        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }


}