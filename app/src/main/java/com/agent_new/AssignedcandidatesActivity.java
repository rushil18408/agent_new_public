package com.agent_new;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import adapters.Adapter_assigned;
import constants.IntentHelper;
import model.AssignedTask;
import model.RecruiterTask;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import services.CheckAndSend;
import services.FileDeleter;
import services.TService;
import utils.AppPreference;

import static com.agent_new.MainActivity.TASK_TAG_PERIODIC;

public class AssignedcandidatesActivity extends AppCompatActivity implements RecruiterPositionInterface {
    PieChart pieChart;
    private int[] yValues = {0, 0};
    private String[] xValues = {"one", "two"};
    public ArrayList<AssignedTask> recruiterList = new ArrayList<>();
    public ArrayList<AssignedTask> filteredList = new ArrayList<>();
    public Long recruiternumber;
    public String LOG_TAG = "AssignedcandidatesActivity";
    public String class_name = "assigned";
    public String res = "";
    public Long userid;
    public Intent intent;
    public String url1 = "http://159.89.162.247:8080/walkinapp/api/usersvc/getUserDetailByContactNo";
    public String url2 = "http://159.89.162.247:8080/walkinapp/api/userSorterByRecruiter/sortUsersForRec";
    public static final int[] MY_COLORS = {
            Color.rgb(0, 0, 0), Color.rgb(211, 211, 211)
    };
    public Adapter_assigned adapter;
    public SearchView searchView;
    public String phone;
    public String uid,recruiterId,requirementId;
    public GcmNetworkManager mGcmNetworkManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_drawer);
        setPieChart();


        String extraStr;
        try {
            extraStr = getIntent().getExtras().getString("Done");
            Log.d("extra",extraStr);
        } catch (NullPointerException e ) {
            extraStr = "something_else";
            Log.d("extra",extraStr);
        }
        if(extraStr.equals("Done")){
            Toast.makeText(this,"CANDIDATE PROFILE HAS BEEN COMPLETED",Toast.LENGTH_LONG).show();
        }
//        Intent intental = getIntent();
//        if(intental.hasExtra("Done")){
//            Toast.makeText(this,"CANDIDATE PROFILE HAS BEEN COMPLETED",Toast.LENGTH_LONG).show();
//        }


        RecyclerView recyclerView = findViewById(R.id.rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new Adapter_assigned(AssignedcandidatesActivity.this, filteredList,this);
        recyclerView.setAdapter(adapter);
        recruiternumber = Long.parseLong(AppPreference.getPhone(this));

        mGcmNetworkManager = GcmNetworkManager.getInstance(this);
        startPeriodicTaskFileUpload();

//        recruiternumber = Long.parseLong("9625322065");

        try {

            post(url1, recruiternumber);

        } catch (IOException e) {
            Toast.makeText(AssignedcandidatesActivity.this, "COULD NOT LOAD DATA HIT BUTTON", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.dhoond_assg);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        final ImageButton home = (ImageButton) findViewById(R.id.home_assg);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        final NavigationView navigationView = findViewById(R.id.nav_view);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);

            }
        });

        final NavigationView navView = findViewById(R.id.nav_view);
        View headerView = navView.getHeaderView(0);
        final Switch scroll = headerView.findViewById(R.id.callrec);
        final FloatingActionButton stop = findViewById(R.id.stop);

        Button goto_next1 = headerView.findViewById(R.id.button_todo_main);
        Button goto_next2 = headerView.findViewById(R.id.button_crm_main);

        goto_next2.setVisibility(View.VISIBLE);
        goto_next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenti = new Intent(AssignedcandidatesActivity.this, MainActivity.class);
                startActivity(intenti);
                finish();
            }
        });


        scroll.setChecked(false);
//        if(scroll.isChecked()){
//            if(iter==0){
//                intent = new Intent(this, TService.class);
//                intent.putExtra("stop", "false");
//                startService(intent);
//                stop.show();
//
//            }
//        }

        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!scroll.isChecked()){
                    stopService(intent);
                    stop.hide();

                }else if(scroll.isChecked()){
                    intent = new Intent(AssignedcandidatesActivity.this, TService.class);
                    intent.putExtra("stop", "false");
                    intent.putExtra("id",AppPreference.getrecruitmentid(AssignedcandidatesActivity.this));
                    startService(intent);
                    stop.show();
                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intent);
                scroll.setChecked(false);
                stop.hide();
            }
        });




        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.isEmpty()) {
                    filteredList.clear();
                    for (AssignedTask job : recruiterList){
                        filteredList.add(job);
                    }
//                    filteredList = recruiterList;
                } else {
                    filteredList.clear();
                    for (AssignedTask job : recruiterList) {

                        if(job.getName().toLowerCase().startsWith(query.toLowerCase())){
                            filteredList.add(job);
//                            ids.add()

                        }
                    }
                }
                adapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (query.isEmpty()) {
                    filteredList.clear();
                    for (AssignedTask job : recruiterList){
                        filteredList.add(job);
                    }
//                    filteredList = recruiterList;
                } else {
                    filteredList.clear();
                    for (AssignedTask job : recruiterList) {

                        if(job.getName().toLowerCase().startsWith(query.toLowerCase())){
                            filteredList.add(job);
//                            ids.add()

                        }
                    }
                }
                adapter.notifyDataSetChanged();
                return false;
            }
        });

        View head= navView.getHeaderView(0);
        final Switch msg = headerView.findViewById(R.id.msg);
        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!msg.isChecked()){
                    Log.d("stopped"," msg");
                    mGcmNetworkManager.cancelAllTasks(CheckAndSend.class);
                }else if(msg.isChecked()){
                    Log.d("started"," msg");
                    startPeriodicTaskFileUpload();
                    startPeriodicTaskSmsSender();
                }
            }
        });



    }

    private void post(String url, Long number) throws IOException {

        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("phoneNo", number.toString())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                datasetterfromjson(res);
                Log.d("data from api 1", res);
                post1(url2,userid);


            }
        });

    }

    public void datasetterfromjson(String json) {
        try {

            JSONObject jsonObject = new JSONObject(json);

            JSONObject response = jsonObject.optJSONObject("response");
            userid = response.optLong("id");

        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

    private void post1(String url, Long uid) {
        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("recId", userid.toString())
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                res = response.body().string();
                datasetterfromjson1(res);
                Log.d("data from api 2", res);
            }
        });

    }

    public void datasetterfromjson1(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jobs = jsonObject.optJSONArray("response");
            String name;
            String status;
            String loc;
            String post;
            String phone;
            String from_where;
            String type;
            String walkinid;
            String hiringid;

            Log.d("length of "," " + jobs.length());
            for (int i = 0; i < jobs.length(); i++) {
                JSONObject jobdata = jobs.optJSONObject(i);
                name = jobdata.optString("userName");
                status = jobdata.optString("client");
                loc = jobdata.optString("location");
                post = jobdata.optString("position");
                phone = jobdata.optString("phone");
                type = jobdata.optString("reason");
                walkinid = jobdata.optString("walkinId");
                uid = jobdata.optString("uid");
                recruiterId = jobdata.optString("recruiterId");
                requirementId = jobdata.optString("requirementId");
                hiringid = jobdata.optString("thruId");

                from_where = jobdata.optString("comingFrom");

                Log.d("data parsed", "ye aaya"+name + " " + status +" "+loc+" "+post+" "+phone);
                AssignedTask Task = new AssignedTask(name,post,status,loc,phone,from_where,type,uid,recruiterId,requirementId,walkinid,hiringid);
                recruiterList.add(Task);
                filteredList.add(Task);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
//                recruiterAdapter.notifyItemInserted(i);
            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }

    public void setPieChart() {
        PieChart pieChart = findViewById(R.id.piechart_3);


        pieChart.setUsePercentValues(true);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.parseColor("#ffffff"));
        pieChart.setHoleRadius(85);
        pieChart.setCenterText("18");
        pieChart.setCenterTextSize(18);
        pieChart.setCenterTextColor(Color.rgb(105, 105, 105));
        pieChart.setDescriptionTextSize(0);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.setDescription(null);


        ArrayList NoOfEmp = new ArrayList();

        NoOfEmp.add(new Entry(18f, 0));
        NoOfEmp.add(new Entry(82f, 1));
        PieDataSet dataSet = new PieDataSet(NoOfEmp, "");
        dataSet.setDrawValues(false);

        ArrayList year = new ArrayList();

        year.add("");
        year.add("");

        PieData data = new PieData(year, dataSet);
        pieChart.setData(data);
        dataSet.setColors(MY_COLORS);
        pieChart.animateXY(3000, 3000);

        PieChart pieChart2 = findViewById(R.id.piechart_2);


        pieChart2.setUsePercentValues(true);
        pieChart2.setDrawHoleEnabled(true);
        pieChart2.setHoleColor(Color.parseColor("#ffffff"));
        pieChart2.setHoleRadius(85);
        pieChart2.setCenterText("40");
        pieChart2.setCenterTextSize(18);
        pieChart2.setCenterTextColor(Color.rgb(105, 105, 105));
        pieChart2.setDescriptionTextSize(0);
        pieChart2.getLegend().setEnabled(false);
        pieChart2.setDrawSliceText(false);
        pieChart2.setDescription(null);

        ArrayList NoOfEmps = new ArrayList();

        NoOfEmps.add(new Entry(40f, 0));
        NoOfEmps.add(new Entry(60f, 1));
        PieDataSet dataSets = new PieDataSet(NoOfEmps, "");
        dataSets.setDrawValues(false);

        ArrayList years = new ArrayList();

        years.add("");
        years.add("");

        PieData datas = new PieData(years, dataSets);
        pieChart2.setData(datas);
        dataSets.setColors(MY_COLORS);
        pieChart2.animateXY(3000, 3000);


        PieChart pieChart3 = findViewById(R.id.piechart_1);


        pieChart3.setUsePercentValues(true);
        pieChart3.setDrawHoleEnabled(true);
        pieChart3.setHoleColor(Color.parseColor("#ffffff"));
        pieChart3.setHoleRadius(85);
        pieChart3.setCenterText("2");
        pieChart3.setCenterTextSize(18);
        pieChart3.setCenterTextColor(Color.rgb(105, 105, 105));
        pieChart3.setDescriptionTextSize(0);
        pieChart3.getLegend().setEnabled(false);
        pieChart3.setDrawSliceText(false);
        pieChart3.setDescription(null);

        ArrayList NoOfEmpss = new ArrayList();

        NoOfEmpss.add(new Entry(90f, 0));
        NoOfEmpss.add(new Entry(10f, 1));
        PieDataSet dataSetss = new PieDataSet(NoOfEmpss, "");
        dataSetss.setDrawValues(false);

        ArrayList yearss = new ArrayList();

        yearss.add("");
        yearss.add("");

        PieData datass = new PieData(yearss, dataSetss);
        pieChart3.setData(datass);
        dataSetss.setColors(MY_COLORS);
        pieChart3.animateXY(3000, 3000);
    }

    @Override
    public void getRecruiterItemPosition(int position, String lol) {
        AppPreference.removeuid(AssignedcandidatesActivity.this);
        AppPreference.settempuid(AssignedcandidatesActivity.this,filteredList.get(position).uid);
        AppPreference.removename(AssignedcandidatesActivity.this);
        AppPreference.settempname(AssignedcandidatesActivity.this,filteredList.get(position).name);
        AppPreference.removerecruiterId(AssignedcandidatesActivity.this);
        AppPreference.settemprecruiterId(AssignedcandidatesActivity.this,filteredList.get(position).recruiterId);
        AppPreference.removerequirementId(AssignedcandidatesActivity.this);
        AppPreference.settemprequirementId(AssignedcandidatesActivity.this,filteredList.get(position).requirementId);
        AppPreference.removetempphone(AssignedcandidatesActivity.this);
        AppPreference.settempcandphone(AssignedcandidatesActivity.this,filteredList.get(position).phone);
        AppPreference.removetempwalkinid(AssignedcandidatesActivity.this);
        AppPreference.settempwalkinid(AssignedcandidatesActivity.this,filteredList.get(position).walkinid);
        AppPreference.removetemphiringid(AssignedcandidatesActivity.this);
        AppPreference.settemphiringid(AssignedcandidatesActivity.this,filteredList.get(position).hiringid);
//        Toast.makeText(this,"u clicked "+ filteredList.get(position).getName(),Toast.LENGTH_LONG).show();
    }

    private void startPeriodicTaskSmsSender() {
        Log.d(LOG_TAG, " started SMS service");
        PeriodicTask task = new PeriodicTask.Builder()
                .setService(CheckAndSend.class)
                .setTag(TASK_TAG_PERIODIC)
                .setFlex(3590L)
                .setPeriod(3600L)
                .setPersisted(true)
                .build();
        mGcmNetworkManager.schedule(task);
    }

    private void startPeriodicTaskFileUpload() {
        Log.d(LOG_TAG, " started File Upload");
        PeriodicTask task = new PeriodicTask.Builder()
                .setService(FileDeleter.class)
                .setTag(TASK_TAG_PERIODIC)
                .setFlex(3590L)
                .setPeriod(3600L)
                .setPersisted(true)
                .build();
        mGcmNetworkManager.schedule(task);
    }
}


