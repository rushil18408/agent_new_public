package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agent_new.R;
import com.agent_new.RecruiterPositionInterface;

import java.util.ArrayList;

import model.SmsTask;

public class Adapter_msg extends RecyclerView.Adapter<Adapter_msg.ReminderHolder>{

    private Context mContext;
    private ArrayList<SmsTask> mRecruiterList;
    private RecruiterPositionInterface mPositionInterface;

    public Adapter_msg(Context context, ArrayList<SmsTask> recruiterList,RecruiterPositionInterface positionInterface) {
        mContext = context;
        mRecruiterList = recruiterList;
        mPositionInterface = positionInterface;

    }

    @NonNull
    @Override
    public ReminderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.msg_row,parent,false);
        return new ReminderHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ReminderHolder holder, int position) {
        SmsTask candidateTask = mRecruiterList.get(position);
        holder.name.setText(candidateTask.head);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPositionInterface.getRecruiterItemPosition(holder.getAdapterPosition(),"LOL");
                int greenColorValue = Color.parseColor("#008577");
                holder.itemView.setBackgroundColor(greenColorValue);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (mRecruiterList != null) {
            return mRecruiterList.size();
        } else {
            return 0;
        }
    }

    class ReminderHolder extends RecyclerView.ViewHolder {

        final TextView name;

        ReminderHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.msg);
            //                Typeface typeface = Typeface.createFromFile();
            //                pending.setTypeface(typeface);
        }
    }

}