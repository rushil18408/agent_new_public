package com.agent_new;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import constants.IntentHelper;
import de.hdodenhof.circleimageview.CircleImageView;
import model.RecruiterInfo;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import utils.AppPreference;

public class ProfileActivity extends AppCompatActivity {
    String subject ="plz click on this link";
    String share = "https://wa.me/918130138055";
    EditText job_comments;
    String candidate_uid;
    String old=" ";
    String phone,name;
    public TextView tcand_email,tcand_number,tcand_whatsapp,tcand_age,tcand_gender,tcand_com,tcand_qual,tcand_experience,tcand_ctc,tcand_designation,tcand_notice,tcand_father,tcand_home,tcand_current,tcand_name,tcand_current_employer;
    String LOG_TAG = "ProfileActivity";
    String cand_email,cand_number,cand_whatsapp,cand_age,cand_gender,cand_com,cand_qual,cand_experience,cand_ctc,cand_designation,cand_notice,cand_father,cand_home,cand_current,cand_current_employer;
    public String url1 = "http://159.89.162.247:8080/walkinapp/api/usersvc/getUserDetailByContactNo";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        phone = AppPreference.gettempcandphone(this);
        cand_number = AppPreference.gettempcandphone(this);
        name = AppPreference.gettempname(this);

        tcand_email = findViewById(R.id.mail_id_profile);
        tcand_number = findViewById(R.id.phone_profile);
        tcand_whatsapp = findViewById(R.id.whatsapp_profile);
        tcand_age = findViewById(R.id.age_profile);
        tcand_gender = findViewById(R.id.gender_profile);
        tcand_com = findViewById(R.id.com_skills_profile);
        tcand_qual = findViewById(R.id.qualification_profile);
        tcand_experience = findViewById(R.id.experience_profile);
        tcand_ctc = findViewById(R.id.CTC_profile);
        tcand_designation = findViewById(R.id.designation_profile);
        tcand_notice = findViewById(R.id.notice_period_profile);
        tcand_father = findViewById(R.id.fathers_name_profile);
        tcand_home = findViewById(R.id.home_city_profile);
        tcand_current = findViewById(R.id.current_location_profile);
        tcand_name = findViewById(R.id.cand_name_profile);
        tcand_current_employer = findViewById(R.id.current_employer_profile);

        ImageButton back = findViewById(R.id.back_profile);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        try {
            post(url1,phone);
        } catch (IOException e) {
            Toast.makeText(this, "COULD NOT LOAD DATA CHECK CONNECTION", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }



        candidate_uid = AppPreference.gettempuid(this);

        job_comments = findViewById(R.id.comments_profileact);
        job_comments.setHint(AppPreference.getcomment(this, candidate_uid, IntentHelper.PROFILE_COMMENT));

        old = AppPreference.getcomment(this, candidate_uid, IntentHelper.PROFILE_COMMENT);
        old=old+" ";
        job_comments.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String newer = s.toString();
                String finals;
                finals = old + newer;
                AppPreference.setcomment(ProfileActivity.this, candidate_uid, IntentHelper.PROFILE_COMMENT, finals);
                job_comments.setHint(finals);
                job_comments.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ImageButton profile = findViewById(R.id.back_profile);
        ImageButton share_botlink = findViewById(R.id.share_profile_botlink);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
        share_botlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                share();
            }
        });
    }
    public void share(){
        Intent my = new Intent(Intent.ACTION_SEND);
        my.setType("text/plain");
        my.putExtra(Intent.EXTRA_SUBJECT,subject);
        my.putExtra(Intent.EXTRA_TEXT,share);
        startActivity(Intent.createChooser(my,"share using :"));


    }

    private void post(String url, String number) throws IOException {

        OkHttpClient client = new OkHttpClient();
        final MediaType FORM = MediaType.parse("multipart/form-data");
        RequestBody formBody = new FormBody.Builder()
                .add("phoneNo", number)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "some error happened in okhttp");
                Log.e(LOG_TAG, e.toString());
                //               Toast.makeText(this,"check internet",0).show();


            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String res = response.body().string();
                datasetterfromjson(res);
                Log.d("data from api 1", res);


            }
        });

    }

    public void datasetterfromjson(String json) {
        try {

            JSONObject jsonObject = new JSONObject(json);
            JSONObject response = jsonObject.optJSONObject("response");
            cand_age = response.optString("age"," ");
            cand_gender = response.optString("gender"," ");
            cand_com = response.optString("commSkills"," ");
            cand_qual = response.optString("qualificationBracket"," ");
            cand_experience = response.optString("experienceBracket"," ");
            cand_notice = response.optString("noticePeriod"," ");
            cand_ctc = response.optString("salary"," ");
            cand_designation = response.optString("userDesignation"," ");
            cand_father = response.optString("fathersName"," ");
            cand_current = response.optString("currentLocation"," ");
            cand_home = response.optString("homeCity"," ");
            cand_current_employer = response.optString("companyName"," ");

            if(cand_age=="null" || cand_age== ""){
                cand_age="-";
            }
            if(cand_gender=="null" || cand_gender== ""){
                cand_gender="-";
            }
            if(cand_com=="null" || cand_com== ""){
                cand_com="-";
            }
            if(cand_qual=="null" || cand_qual== ""){
                cand_qual="-";
            }
            if(cand_experience=="null" || cand_experience== ""){
                cand_experience="-";
            }
            if(cand_notice=="null" || cand_notice== ""){
                cand_notice="-";
            }
            if(cand_ctc=="null" || cand_ctc== ""){
                cand_ctc="-";
            }
            if(cand_designation=="null" || cand_designation== ""){
                cand_designation="-";
            }
            if(cand_father=="null" || cand_father== ""){
                cand_father="-";
            }
            if(cand_current=="null" || cand_current== ""){
                cand_current="-";
            }
            if(cand_home=="null" || cand_home== ""){
                cand_home="-";
            }
            if(cand_current_employer=="null" || cand_current_employer== ""){
                cand_current_employer="-";
            }
            String mail=" ";

            JSONArray email = response.optJSONArray("userEmailColl");
            for (int i = 0; i < email.length(); i++){
                JSONObject em = email.getJSONObject(i);
                mail = em.optString("email");

            }

            cand_email = mail;
            if(cand_email=="null" || cand_email== ""){
                cand_email="-";
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tcand_age.setText(cand_age);
                    tcand_gender.setText(cand_gender);
                    tcand_com.setText(cand_com);
                    tcand_qual.setText(cand_qual);
                    tcand_experience.setText(cand_experience);
                    tcand_notice.setText(cand_notice);
                    tcand_ctc.setText(cand_ctc);
                    tcand_designation.setText(cand_designation);
                    tcand_father.setText(cand_father);
                    tcand_current.setText(cand_current);
                    tcand_home.setText(cand_home);
                    tcand_email.setText(cand_email);
                    tcand_number.setText(phone);
                    tcand_whatsapp.setText(phone);
                    tcand_name.setText(name);
                    tcand_current_employer.setText(cand_current_employer);
                }
            });


        } catch (JSONException e) {
            Log.d(LOG_TAG, "COULD NOT PARSE JSON");
            e.printStackTrace();
        }

    }
}
