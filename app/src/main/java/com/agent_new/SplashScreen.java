package com.agent_new;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;

public class SplashScreen extends AppCompatActivity {
    Button todo , crm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        todo = findViewById(R.id.todo_splash);

        todo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ccco = new Intent(SplashScreen.this,AssignedcandidatesActivity.class);
                startActivity(ccco);
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LottieAnimationView animationView = findViewById(R.id.loading);
                animationView.pauseAnimation();
                animationView.setVisibility(View.GONE);
                finish();
            }
        }, 4000);


    }
}
