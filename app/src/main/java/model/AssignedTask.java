package model;

public class AssignedTask {
    public String name ;
    public String status ;
    public String loc;
    public String post;
    public String phone;
    public String from_where;
    public String type;
    public String uid;
    public String recruiterId;
    public String requirementId;
    public String walkinid;
    public String hiringid;


    public AssignedTask(String name, String day ,String loc,String post,String ph,String from,String type,String uid,String recruiterId,String requirementId,String walkinid,String hiringid){
        this.name = name;
        this.status = day;
        this.loc = loc;
        this.post = post;
        if(ph.length()>10){
            ph="+"+ph;
        }
        this.phone = ph;
        this.from_where = from;
        this.type = type;
        this.uid = uid;
        this.recruiterId = recruiterId;
        this.requirementId = requirementId;
        this.walkinid=walkinid;
        this.hiringid=hiringid;

    }
    public String getPhone(){
        return(this.phone);
    }
    public String getName(){
        return(this.name);
    }
    public String getUid(){
        return(this.uid);
    }
    public String getLoc(){
        return(this.loc);
    }
    public String getPost(){
        return(this.post);
    }
    public String getDay(){
        return(this.status);
    }
    public void setPhone(String h){
        this.phone = h ;
    }
    public void setName(String h){
        this.name = h ;
    }
    public void setUid(String h){
        this.uid = h ;
    }
    public void setDay(String h){
        this.status = h ;
    }
    public void setLoc(String h){
        this.loc = h ;
    }
    public void setPost(String h){
        this.post= h ;
    }

}

