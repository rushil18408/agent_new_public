package constants;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by rushil .
 */
public class StringUtilLocal {

    public static String EMPTY = "";
    public static String SINGLE_SPACE = " ";
    public static String ZERO = "0";
    public static String PLUS_91 = "+91";
    public static String COMMA = ",";
    public static String QUERY_DEEP_LINKING_EMPTY = "query_deep_linking_empty";
    public static String SERVICE_START_STOP = "service_start_stop";

    private static final char EXTENSION_SEPARATOR = '.';

    private static final char UNIX_SEPARATOR = '/';


    public static final char NEXT_LINE = '\n';

    /**
     * The Windows separator character.
     */
    private static final char WINDOWS_SEPARATOR = '\\';

    private static String removeExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    public static String getBaseName(String filename) {
        return removeExtension(getName(filename));
    }

    private static String getName(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfLastSeparator(filename);
        return filename.substring(index + 1);
    }

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    private static int indexOfExtension(String filename) {
        if (filename == null) {
            return -1;
        }
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        int lastSeparator = indexOfLastSeparator(filename);
        return (lastSeparator > extensionPos ? -1 : extensionPos);
    }


    private static int indexOfLastSeparator(String filename) {
        if (filename == null) {
            return -1;
        }
        int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
        int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
        return Math.max(lastUnixPos, lastWindowsPos);
    }

    public static boolean isEmptyAndStringNullCheck(String value) {
        return TextUtils.isEmpty(value) || value.equalsIgnoreCase("null");
    }

    public static boolean isArrayEmpty(List data) {
        return data == null || data.isEmpty();
    }

    public static boolean isJSONArrayIsEmpty(JSONArray jsonArray) {
        return jsonArray == null || jsonArray.length() == 0;
    }

    public static boolean isJSONObjectIsEmpty(JSONObject jsonObject) {
        return jsonObject == null || jsonObject.length() == 0;
    }
}
