package services;


import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.telephony.SmsManager;
import android.util.Log;

import com.agent_new.MainActivity;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

import java.util.HashMap;
import java.util.Map;

public class CheckAndSend extends GcmTaskService {

    private static final String LOG_TAG = CheckAndSend.class.getSimpleName();

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        doPeriodicTask();
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.d(LOG_TAG, "onRunTask: " + taskParams.getTag());
        String tag = taskParams.getTag();
        int result = GcmNetworkManager.RESULT_SUCCESS;
        if (MainActivity.TASK_TAG_PERIODIC.equals(tag)) {
            result = doPeriodicTask();
        }
        return result;
    }

    private int doPeriodicTask() {
        Log.e(LOG_TAG, "periodic task executed");

        long ek_ghante_pehle_ka_samay = System.currentTimeMillis();
        ek_ghante_pehle_ka_samay=ek_ghante_pehle_ka_samay-3600000;
        Uri allMessages = Uri.parse("content://sms/sent");
        ContentResolver contentResolver = getContentResolver();

        Cursor messageCursor = contentResolver.query(allMessages, null, null, null, null);
        Map< String,Integer> hm = new HashMap<>();
//        while (messageCursor != null && messageCursor.moveToNext()) {
//            String no = messageCursor.getString(messageCursor.getColumnIndexOrThrow("address"));
//            hm.put(no,1);
//            Log.d(LOG_TAG,no + "checked this for sent messages ");
//        }
        if(messageCursor != null)
        messageCursor.close();

        //
        long samay;
        boolean duaon_main_yad_rakhna = true;
        Uri callUri = Uri.parse("content://call_log/calls");
        Cursor callLogCursor = contentResolver.query(callUri, null, null, null, "DATE DESC");
        while (callLogCursor != null && callLogCursor.moveToNext() && duaon_main_yad_rakhna) {
            String num = callLogCursor.getString(callLogCursor.getColumnIndexOrThrow("number"));
            int type  = Integer.parseInt(callLogCursor.getString(callLogCursor.getColumnIndex(CallLog.Calls.TYPE)));
            String date = callLogCursor.getString(callLogCursor.getColumnIndexOrThrow("date"));
            Log.e("checked",num + " " + date+"system time"+ek_ghante_pehle_ka_samay);
            samay = Long.parseLong(date);
            if(samay<ek_ghante_pehle_ka_samay){
                duaon_main_yad_rakhna=false;
            }
            if(type == 3){
                Log.d("CheckAndSend",num+"is a missed call ");
                Log.d("CheckAndSend","**********************");
                if(!hm.containsKey(num)){
                   Log.d("CheckAndSend",num + "sent message  to this");
                   sendSMS(num);
                   hm.put(num,1);
               }
            }
        }
        if(callLogCursor != null)
        callLogCursor.close();
        return GcmNetworkManager.RESULT_SUCCESS;
    }
    private void sendSMS(String sendToNumber) {
        if (sendToNumber != null) {
            String message = utils.AppPreference.getSmsText(this);
            if (message == null || message.isEmpty()) {
                message = "I am busy right now and can't talk. Please contact me on WhatsApp" + " - " + "https://wa.me/918130138055";
            }
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(sendToNumber, null, message, null, null);
        } else {
            Log.e(LOG_TAG, "Wrong number has been entered ");
        }
    }

}