package model;

public class CandidateTask {
    public String name ;
    public String day ;
    public String current_city;
    public String home_city;
    public String ctc;
    public String qualification;
    public Long numbers;

    public CandidateTask(String name, String day ,String current,String home,String ctc,String qualification,Long number){
        this.name = name;
        this.day = day;
        this.current_city=current;
        this.home_city=home;
        this.ctc=ctc;
        this.qualification=qualification;
        this.numbers = number;

    }
    public String getName(){
        return(this.name);
    }
    public String getCurrent_city(){
        return(this.current_city);
    }
    public String getHome_city(){
        return(this.home_city);
    }
    public String getCtc(){
        return(this.ctc);
    }
    public String getQualification(){
        return(this.qualification);
    }
    public String getDay(){
        return(this.day);
    }
    public Long getNumber(){ return(this.numbers);}

    public void setName(String h){
        this.name = h ;
    }
    public void setCurrent_city(String h){
        this.current_city = h ;
    }
    public void setHome_city(String h){
        this.home_city = h ;
    }
    public void setCtc(String h){
        this.ctc= h ;
    }
    public void setQualification(String h){
        this.qualification = h ;
    }
    public void setDay(String h){
        this.day = h ;
    }
    public void setNumbers(Long number){ this.numbers = number ; }

}
